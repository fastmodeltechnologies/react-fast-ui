#React Fast UI

A library of layout-related components for re-use 
across all Fastmodel React applications.


## Installation
```
yarn add react-fast-ui
```

## Usage

You must import the CSS file for the sidebar and modal 
to work properly. Most other styles are inline. Then 
you can import the individual components you need.

```jsx
import 'react-fast-ui/css/index.css';
// e.g.
import { Modal, Sidebar } from 'react-fast-ui';
```

## Contribution

If you are going to add a component to this library, 
make sure that it is universally useful to our apps. Then follow these steps to 
 develop your component:
 
1. Run ```npm run demo-build:watch``` from the command line.

2. Open ```index.html``` in your browser.

3. Create your component file in ```src/components```. Export your class and 
make sure that the class has a static ```demo``` function that returns a ```<Demo/>``` 
component.

4. Add your component's class name to the ```COMPONENTS``` array in ```index.js```.

5. Now you can write your component as normal. In order to see what you are working on, 
you will have to update the demo simultaneously. This will allow you to manually test your work 
in isolation in the ```index.html``` page you have open in your browser.
