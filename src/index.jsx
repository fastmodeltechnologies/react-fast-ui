import _ from 'underscore';

const COMPONENTS = [
  // A
  'Alert',

  // B
  'Button',
  'ButtonToggle',

  // C
  'Circle',
  'Collapsible',
  'CornerAngle',
  'Checkbox',

  // D
  'Drawer',

  // F
  'Form',

  // I
  'Icon',
  'IconButton',
  'InverseButton',

  // L
  'LightBox',
  'LinkButton',
  'LoadingWrapper',

  // M
  'Modal',

  // O
  'OnClickOut',

  // P
  'PageHeader',
  'PageTitle',
  'Popover',
  'Portal',

  // S
  'Spinner',
  'Sidebar',
  'SimpleButton',

  // T
  'Table',
  'Tabs',
  'TinySelect',
  'Tip',
  'TeamLogo',
  'TeamHeader'
];

const DEMOS = {};
const COMPONENTS_WITHOUT_DEMOS = {};

_.forEach(COMPONENTS, (componentName) => {
  const Component = require(`./components/${componentName}`).default;

  if (Component.demo) {
    // Add demo to DEMOS object
    DEMOS[ componentName ] = Component.demo;

    // Delete demo from Component that will be exported
    delete Component.demo;
  }

  // Add Component (sans demo) to COMPONENTS_WITHOUT_DEMOS object
  COMPONENTS_WITHOUT_DEMOS[ componentName ] = Component;
});

module.exports = {
  DEMOS,
  ...COMPONENTS_WITHOUT_DEMOS,
};