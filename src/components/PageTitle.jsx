import React, { Children, PureComponent } from 'react';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import _ from 'underscore';

export default class PageTitle extends PureComponent {
  static propTypes = {
    appName: PropTypes.string,
    title: PropTypes.string,
  };

  render() {
    const { appName, title, ...rest } = this.props;

    return (
      <DocumentTitle title={_.compact([ appName, title ]).join(' | ')} {...rest}/>
    );
  };
}