import React, { Component, cloneElement, PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Icon from './Icon';

class PageSubHeader extends PureComponent {
  render() {
    const { children, className, style } = this.props;
    const props = {
      className: cx(
        'bg-color-blue-gray',
        'full-width',
        'pd-bottom-sm',
        'pd-top-sm',
        className
      ),
      style: {
        minHeight: 42,
        ...style
      }
    };

    return <div {...props}>{children}</div>;
  }
}

const ICON_STYLES = { bottom: -24, marginLeft: 20, zIndex: -1 };

const HEADER_CLASSES = cx(
  'display-flex',
  'align-items-flex-end',
  'justify-content-space-between',
  'max-width',
  'sm-down-flex-direction-column',
  'sm-down-align-items-center',
  'mg-bottom-sm'
);

const H1_CLASSES = cx(
  'display-flex',
  'flex-grow-1',
  'full-height',
  'align-items-flex-end',
  'pd-bottom-sm',
  'mg-none',
  'position-relative'
);

export default class PageHeader extends Component {
  static Subhead = PageSubHeader;

  static propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
    bottomOffsetPixels: PropTypes.number,
    iconSize: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  };

  static defaultProps = {
    bottomOffsetPixels: 20,
    icon: 'dribbble fa-rotate-56',
    iconSize: 100,
  };

  getIconStyle() {
    const { bottomOffsetPixels, iconSize } = this.props;

    return {
      ...ICON_STYLES,
      bottom: `${bottomOffsetPixels * -1}px`,
      fontSize: iconSize
    };
  }

  render() {
    const { children, className, icon, title } = this.props;

    return (
      <header className={cx('hidden-print', className)}>
        <div className='mg-bottom-sm'>
          <section key='head' className={HEADER_CLASSES} style={{ minHeight: 120 }}>
            <h1 key='left' className={H1_CLASSES}>
              {title}
              <Icon
                name={icon}
                style={this.getIconStyle()}
                className='position-absolute color-lightest-gray hidden-xs'
              />
            </h1>
            <aside key='right' className='pd-bottom-sm'>
              {children}
            </aside>
          </section>
        </div>
      </header>
    );
  }
}
