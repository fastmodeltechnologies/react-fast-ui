import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export default class CornerAngle extends PureComponent {
  static propTypes = {
    // direction? Don't need it now, but could be a nice enhancement
    colorClass: PropTypes.string,
    rgb: PropTypes.string,
    size: PropTypes.oneOf([ 'xs', 'sm', 'md', 'lg', 'xl' ])
  };

  static defaultProps = {
    colorClass: 'primary-color',
    rgb: null,
    size: 'md'
  };

  getClasses() {
    const { className, colorClass, size } = this.props;

    return cx(
      `border-width-${size}`,
      (colorClass || null),
      className
    );
  }

  getBorderStyles() {
    const { rgb } = this.props;

    let styles = {
      borderTopColor: 'transparent',
      borderRightStyle: 'solid',
      borderTopStyle: 'solid'
    };

    if (rgb) {
      styles.borderRightColor = `rgb(${rgb})`;
    }

    return styles;
  }

  render() {
    return <div className={this.getClasses()} style={this.getBorderStyles()}></div>;
  }
}
