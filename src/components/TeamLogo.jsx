// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';

const BLANK_PLACEHOLDER = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

export default class TeamLogo extends Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    height: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
    width: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ])
  };

  static defaultProps = {
    height: 100,
    width: 'auto'
  };

  state = {
    errorPlaceHolder: false,
  };

  getDimensions = () => {
    const { height, width, style, fullSizePlaceholder } = this.props;
    let maxWidth;
    let placeholder;

    if (width === 'auto') {
      maxWidth = 2 * height;
    }

    placeholder = fullSizePlaceholder && this.state.errorPlaceHolder ? { marginRight: 10 } : {};

    if (this.state.errorPlaceHolder && !fullSizePlaceholder) {
      return { width: 0, height: 0 };
    }

    return { fontSize: height, height, width, maxWidth, objectFit: 'scale-down', padding: 2, ...style, ...placeholder };
  };

  // lifecycle
  render() {
    return <img
      src={this.state.errorPlaceHolder || this.props.url}
      onError={e => {
        e.target.onerror = null;
        this.setState({ errorPlaceHolder: BLANK_PLACEHOLDER });
      }}
      style={this.getDimensions()}
    />;
  }
}