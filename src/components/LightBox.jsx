import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Button from './Button';

const DEMO_SRC = 'http://media.gettyimages.com/photos/dee-brown-deron-williams-and-luther-head-of-the-illinois-fighting-picture-id165540247'; //eslint-disable-line

const BACKGROUND_CLASSES = classnames(
  'position-fixed',
  'cursor-pointer',
  'left-none',
  'z-index-modal',
  'full-width',
  'full-height',
  'bg-color-transparent-shade',
  'pd-md'
);

const BUTTON_CLASSES = classnames(
  'position-absolute',
  'align-items-center',
  'display-flex',
  'pd-right-lg',
  'right-lg',
  'top-lg',
  'color-white'
);

const IMAGE_CLASSES = classnames(
  'position-absolute',
  'bg-color-white',
  'transform-translate-minus-50-minus-50'
);

const IMAGE_STYLES = {
  boxShadow: '0 0 1px #222',
  left: '50%',
  top: '50%',
  maxHeight: '100%',
  maxWidth: '100%'
};

export default class LightBox extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    src: PropTypes.string.isRequired,
    topOffset: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ])
  };

  static defaultProps = {
    topOffset: 0
  };

  render() {
    const { onClose, open, src, topOffset } = this.props;

    if (open) {
      return (
        <div key='showLightbox' className={BACKGROUND_CLASSES}
             style={{ backgroundColor: 'rgba(0, 0, 0, 0.7)', top: topOffset }} onClick={onClose}>
          <Button
            borderless={true}
            className={BUTTON_CLASSES}
            icon='times'
            text='Close'
            onClick={onClose}
            size='lg'
          />
          <img className={IMAGE_CLASSES} src={src} style={IMAGE_STYLES} onClick={e => e.stopPropagation()}/>
        </div>
      );
    } else {
      return null;
    }
  }
}