import React, { Children, cloneElement, PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import cx from 'classnames';
import LinkButton from './LinkButton';

const TAB_KEY_TYPE = PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]);

export const TAB_STYLES = {
  containerStyle: {},
  listStyle: { borderBottom: '1px solid #cecece' },
  listCX: 'list-unstyled pd-none hidden-print mg-bottom-none',
  labelLICX: 'display-inline-block',
  labelLIStyle: { marginRight: 30, padding: '0 10px 8px 10px', position: 'relative', left: '-10px' },
  selectedCX: 'border-bottom-width-4px border-bottom-style-solid border-bottom-color-rich-orange',
  selectedStyle: { color: '#0a0a0a', fontWeight: 800 },
  linkCX: 'color-gray-background-darken',
  linkStyle: {},
  selectedLinkCX: '',
};

export const SUB_TAB_STYLES = {
  containerStyle: {
    width: '100vw',
    position: 'relative',
    left: '50%',
    marginLeft: '-50vw',
    backgroundColor: '#ececec',
    display: 'flex',
    justifyContent: 'center',
    borderBottom: '1px solid #cecece',
    borderTop: '1px solid #cecece',
  },
  listStyle: {},
  listCX: 'list-unstyled pd-sm hidden-print mg-bottom-none',
  labelLICX: 'display-inline-block pd-top-sm pd-bottom-sm',
  labelLIStyle: { paddingRight: '50px' },
  selectedCX: 'pd-none color-orange-darken hover-color-orange-darken font-weight-bolder',
  selectedStyle: {},
  linkCX: 'pd-none color-primary hover-color-orange-darken',
  linkStyle: {},
  selectedLinkCX: 'pd-none color-orange-darken hover-color-orange-darken font-weight-bolder',
};

class Tab extends PureComponent {
  static propTypes = {
    disabled: PropTypes.bool,
    selectedKey: TAB_KEY_TYPE,
    tabKey: TAB_KEY_TYPE,
  };

  static defaultProps = {
    disabled: false,
  };

  isSelected = () => _.isEqual(this.props.tabKey, this.props.selectedKey);

  render() {
    const { className, children, style } = this.props;

    return (!this.isSelected() ? null :
        <div className={className} style={style}>
          {
            Children.only(children)
          }
        </div>
    );
  }
}

export default class Tabs extends PureComponent {
  static Tab = Tab;

  static propTypes = {
    onSelect: PropTypes.func,
    selectedKey: TAB_KEY_TYPE,
    isSubTabs: PropTypes.bool,
  };

  static defaultProps = {
    onSelect: (null: ?Function),
    selectedKey: (null: ?string),
    isSubTabs: false,
  };

  handleClickTab = (tabKey: string) => {
    const { onSelect } = this.props;
    return onSelect ? onSelect(tabKey) : null;
  };

  render() {
    const { className, children, selectedKey, listStyle, isSubTabs, style } = this.props;
    const STYLE = isSubTabs ? SUB_TAB_STYLES : TAB_STYLES;

    return (
      <div style={style}>
        <div style={STYLE.containerStyle}>
          <ul
            className={cx(STYLE.listCX, className)}
            style={{ ...STYLE.listStyle, ...listStyle }}>
            {
              Children.map(children, (child: Object, ix: number) => {
                if (!child) {
                  return null;
                }
                const { disabled, href, label, style, tabKey } = child.props;
                const isSelected = (selectedKey === tabKey);
                const onClick = disabled ? null : () => this.handleClickTab(tabKey);
                const className = isSelected ? STYLE.selectedLinkCX : STYLE.linkCX;

                return (
                  <li
                    key={ix}
                    className={cx(STYLE.labelLICX, { [ STYLE.selectedCX ]: isSelected })}
                    style={{ ...STYLE.labelLIStyle, ...(isSelected ? STYLE.selectedStyle : null), ...style }}>
                    <LinkButton
                      disabled={disabled}
                      href={href}
                      onClick={onClick}
                      size="lg"
                      className={className}>
                      {label}
                    </LinkButton>
                  </li>
                );
              })
            }
          </ul>
        </div>
        {
          Children.map(children, (child: Object) => {
            if (!child) {
              return null;
            }
            return cloneElement(child, { selectedKey });
          })
        }
      </div>
    );
  }
}