import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';

/**
 * Tests if element is contained within the testParent or has the required class.
 * @param child to test for containment
 * @param parent to see if contains child
 * @returns {boolean} true if child is equal to or contained within testParent
 */
function isContained(child, parent, className) {
  // while element points to a valid element
  while (child) {
    // if the element is the parent we are testing for, then it's contained within the testParent
    if (child === parent || (className && child.classList.contains(className))) {
      return true;
    }
    child = child.parentElement;
  }

  return false;
}

export default class OnClickOut extends Component {
  static isContained = isContained;

  static propTypes = {
    onClickOut: PropTypes.func.isRequired,
    // whether the onClickOut event should fire if the target of the event is not in the DOM by the time the event
    // makes it to the window object
    fireOnUnmountedDOM: PropTypes.bool,

    // An optional class that can be provided to check for.  Allows use of popover/portal component that will fail
    // the isContained checking purely parentElements.
    portalClass: PropTypes.string,

    eventType: PropTypes.oneOf([ 'click', 'mousedown', 'mouseup' ])
  };

  static defaultProps = {
    eventType: 'click',
    fireOnUnmountedDOM: false
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.children !== this.props.children;
  }

  windowClickListener = (clickEvent) => {
    const { onClickOut, fireOnUnmountedDOM, portalClass } = this.props;

    // find the DOM node corresponding to this component
    const me = findDOMNode(this);

    // check if the target of the click event is within this DOM node
    const { target } = clickEvent;
    const inPage = isContained(target, document.documentElement);

    if (!isContained(target, me, portalClass) && (fireOnUnmountedDOM || inPage)) {
      onClickOut(clickEvent);
    }
  };

  componentDidMount() {
    const { eventType } = this.props;
    window.addEventListener(eventType, this.windowClickListener);
  }

  componentWillUnmount() {
    const { eventType } = this.props;
    window.removeEventListener(eventType, this.windowClickListener);
  }

  render() {
    const { children } = this.props;
    return Children.count(children) === 1 ? Children.only(children) : <div>{children}</div>;
  }
}