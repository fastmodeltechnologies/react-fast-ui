import { Component, Children } from 'react';
import PropTypes from 'prop-types';
import withSideEffect from 'react-side-effect';
import cx from 'classnames';
import _ from 'underscore';

class BodyClassName extends Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
  };

  render() {
    return Children.only(this.props.children);
  }
}

const reducePropsToState = (propsList) => cx(_.pluck(propsList, 'className').join(' '));
const handleStateChangeOnClient = (className) => document.body.className = cx(document.body.class, className);

export default withSideEffect(
  reducePropsToState,
  handleStateChangeOnClient
)(BodyClassName);