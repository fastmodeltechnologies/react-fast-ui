// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import SimpleButton from './SimpleButton';

const getBgColorClasses = (type: string) => `bg-color-white color-${type}`;
const getBorderClasses = (type: string) => (
  `border-width-1px border-style-solid border-radius-xxl border-color-${type} hover-border-color-${type}-darken`
);
const getDisabledBorderClasses = () => (
  'border-width-1px border-style-solid border-radius-xxl border-color-grey-darken'
);

export default class InverseButton extends Component {
  static sizes = SimpleButton.sizes;
  static types = SimpleButton.types;

  static propTypes = {
    disabled: PropTypes.bool,
    href: PropTypes.string,
    onClick: PropTypes.func,
    size: PropTypes.oneOf(InverseButton.sizes),
    tabIndex: PropTypes.string,
    type: PropTypes.oneOf(InverseButton.types),
  };

  static defaultProps = {
    disabled: false,
    href: (null: ?string),
    onClick: (null: ?Function),
    size: 'md',
    tabIndex: (null: ?string),
    type: 'primary',
  };

  beforeOnClick = (e: SyntheticEvent) => (this.props.onClick ? this.props.onClick(e) : null);

  focus = () => this.refs.button.focus();

  getPaddingClasses = () => {
    const { size } = this.props;
    const verticalPadding = size === 'sm' ? 'xs' : 'sm';

    return `pd-top-${verticalPadding} pd-right-${size} pd-bottom-${verticalPadding} pd-left-${size}`;
  };

  getHoveringClasses = () => {
    const { type, disabled } = this.props;
    if (disabled) {
      return;
    }
    return type !== 'primary' ? `color-${type} hover-color-${type}-darken` : 'hover-bg-color-primary hover-color-white';
  };

  render() {
    const { children, className, disabled, href, size, style, tabIndex, type, ...rest } = this.props;
    const classNames = cx(
      this.getHoveringClasses(),
      'outline-none',
      (disabled ? 'color-light-gray' : getBgColorClasses(type)),
      (disabled ? getDisabledBorderClasses() : getBorderClasses(type)),
      (disabled ? 'cursor-not-allowed' : 'cursor-pointer'),
      `${size}-font`,
      this.getPaddingClasses(),
      className
    );
    const styles = { textDecoration: 'none', ...style };
    const onClick = (e) => (disabled ? null : this.beforeOnClick(e));

    return (
      <a
        {...rest}
        className={classNames}
        style={styles}
        ref="button"
        href={href}
        onClick={onClick}
        tabIndex={tabIndex}>
        {children}
      </a>
    );
  }
}