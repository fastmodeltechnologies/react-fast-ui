import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const ICON_SIZES = [ null, '1x', 'lg', '2x', '3x', '4x', '5x' ];
const ICON_ANIMATES = [ null, 'spin', 'pulse' ];

export default class Icon extends PureComponent {
  static sizes = ICON_SIZES;
  static animates = ICON_ANIMATES;

  static propTypes = {
    name: PropTypes.string.isRequired,
    size: PropTypes.oneOf(ICON_SIZES),
    fixed: PropTypes.bool,
    li: PropTypes.bool,
    border: PropTypes.bool,
    animate: PropTypes.oneOf(ICON_ANIMATES),
    className: PropTypes.string,
    style: PropTypes.object,
  };

  static defaultProps = {
    size: null,
    fixed: false,
    li: false,
    border: false,
    animate: null,
    className: null,
    style: null,
  };

  render() {
    const { name, size, fixed, li, border, className, animate, ...rest } = this.props;

    return (
      <i
        className={cx(
          `fa fa-${name}`,
          {
            'fa-border': border,
            'fa-fw': fixed,
            'fa-li': li,
            [ `fa-${size}` ]: size,
            [ `fa-${animate}` ]: animate
          },
          className
        )}
        {...rest}
      />
    );
  }
}