import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Icon from './Icon';

const BUTTON_SIZES = [ 'sm', 'md', 'lg' ];
const BUTTON_TYPES = [ 'primary', 'secondary', 'default', 'danger', 'success', 'warning' ];
const ICON_CLASSES = cx('display-inline-block', 'vertical-align-middle');

export default class Button extends Component {
  static types = BUTTON_TYPES;
  static sizes = BUTTON_SIZES;

  static propTypes = {
    block: PropTypes.bool,
    borderless: PropTypes.bool,
    className: PropTypes.string,
    clickDelay: PropTypes.number,
    disabled: PropTypes.bool,
    href: PropTypes.string,
    hoverText: PropTypes.bool,
    inverse: PropTypes.bool,
    icon: PropTypes.node,
    loading: PropTypes.bool,
    loadingIcon: PropTypes.node,
    loadingText: PropTypes.node,
    onClick: PropTypes.func,
    size: PropTypes.oneOf(BUTTON_SIZES),
    submit: PropTypes.bool,
    tabIndex: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
    type: PropTypes.oneOf(BUTTON_TYPES),
    text: PropTypes.node,
    style: PropTypes.object,
    buttonIconStyle: PropTypes.object,
    buttonTextStyle: PropTypes.object,
  };

  static defaultProps = {
    block: false,
    borderless: false,
    clickDelay: 200,
    disabled: false,
    href: null,
    hoverText: false,
    inverse: false,
    icon: null,
    loading: false,
    loadingIcon: null,
    loadingText: null,
    onClick: null,
    size: 'md',
    submit: false,
    type: 'default',
    text: null,
    style: null,
    buttonIconStyle: null,
    buttonTextStyle: null,
  };

  state = { lastClick: 0 };

  beforeOnClick(e) {
    const { clickDelay, submit, href, onClick } = this.props;
    const now = (new Date()).getTime();

    // if this isn't a submit button, then
    // prevent the default action of submitting the form
    if (!submit && !href) {
      e.preventDefault();
    }

    if (now - this.state.lastClick <= clickDelay) {
      return false;
    }

    this.setState({ lastClick: now });

    if (onClick) {
      onClick(e);
    }
  }

  focus = () => this.refs.button.focus();

  getBorderClasses() {
    const { borderless } = this.props;
    if (borderless) {
      return 'border-none border-radius-xxl';
    }

    return 'border-width-1px border-style-solid border-radius-xxl';
  }

  getColorClasses() {
    const { disabled, borderless } = this.props;
    let { type, inverse } = this.props;

    // handle the disabled state first since they look the same
    if (disabled) {
      if (borderless) {
        return 'bg-color-transparent color-lightest-gray';
      } else {
        return 'bg-color-light-gray color-white';
      }
    }

    if (type == 'default') {
      type = 'primary';
      inverse = !inverse;
    }

    // the common case
    if (borderless) {
      return `bg-color-transparent color-${type} hover-color-${type}-darken`;
    } else {
      if (inverse) {
        return `bg-color-white color-${type} border-color-${type} hover-bg-color-${type} hover-color-white`;
      } else {
        return `bg-color-${type} hover-bg-color-${type}-darken border-color-${type}-darken color-white`;
      }
    }
  }

  getPaddingClasses() {
    const { size } = this.props;

    let verticalPadding = size === 'sm' ? 'xs' : 'sm';

    return `pd-top-${verticalPadding} pd-right-${size} pd-bottom-${verticalPadding} pd-left-${size}`;
  }

  getButtonClasses() {
    const { disabled, className } = this.props;

    return cx(
      this.getBorderClasses(),
      this.getColorClasses(),
      this.getPaddingClasses(),
      {
        'cursor-pointer': !disabled
      },
      'outline-none',
      'hover-visibility-parent',
      className
    );
  }

  getTextClasses() {
    const { borderless, hoverText, icon, size, text } = this.props;

    const marginClass = icon ? 'mg-left-sm' : '';

    return cx(
      (borderless && !text ? 'display-none' : 'display-inline-block'),
      `${size}-font`,
      {
        'visibility-hidden': hoverText,
        'visibility-child-visible': hoverText
      },
      marginClass
    );
  }

  commonProps() {
    const { disabled, loading, tabIndex } = this.props;

    return {
      className: this.getButtonClasses(),
      disabled: (disabled || loading),
      tabIndex
    };
  }

  renderIcon() {
    const { borderless, icon, loadingIcon, loading, size, buttonIconStyle } = this.props;

    const iconSize = {
      sm: borderless ? '1x' : '1x',
      md: borderless ? 'lg' : 'lg',
      lg: borderless ? '2x' : 'lg',
    };

    let iconToShow = (loading && loadingIcon !== null) ? loadingIcon : icon;

    if (iconToShow) {
      if (typeof iconToShow === 'string') {
        return (
          <Icon key='icon' className={ICON_CLASSES} name={iconToShow}
            size={iconSize[ size ]} style={buttonIconStyle}/>
        );
      } else {
        return iconToShow;
      }
    } else {
      return null;
    }
  }

  renderText() {
    const { text, loadingText, loading, buttonTextStyle } = this.props;
    let spanContent = (loading && loadingText !== null) ? loadingText : text;

    return (
      <span key='text' className={this.getTextClasses()} style={buttonTextStyle}>{spanContent}</span>
    );
  }

  render() {
    const { disabled, href, submit, style } = this.props;

    if (href && !disabled) {
      // Render an <a> if we've passed in an href, and the button is not disabled
      return (
        <a
          ref="button"
          href={href}
          style={style}
          onClick={(e) => this.beforeOnClick(e)}
          {...this.commonProps()}>
          {this.renderIcon()}
          {this.renderText()}
        </a>
      );
    } else {
      // Render a <button> if we've not passed in an href
      return (
        <button
          ref="button"
          style={style}
          onClick={(e) => this.beforeOnClick(e)}
          type={submit ? 'submit' : 'button'}
          {...this.commonProps()}>
          {this.renderIcon()}
          {this.renderText()}
        </button>
      );
    }
  }
}