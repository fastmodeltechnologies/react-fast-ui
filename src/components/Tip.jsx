import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import Popover from './Popover';
import _ from 'underscore';


export default class Tip extends Component {
  static propTypes = {
    tip: PropTypes.node.isRequired
  };

  state = {
    hovering: false
  };

  setHovering = _.debounce((hovering) => {
    this.setState({ hovering });
  }, 100);

  renderTip() {
    const { tip } = this.props;

    return <div className='pd-sm'>{tip}</div>;
  }

  render() {
    const { hovering } = this.state;
    const { children, ...rest } = this.props;

    return (
      <Popover content={this.renderTip()} open={hovering} {...rest}>
        <span onMouseEnter={() => this.setHovering(true)} onMouseLeave={() => this.setHovering(false)}>
          {children}
        </span>
      </Popover>
    );
  }
}