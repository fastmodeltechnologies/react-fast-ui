import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';

const DISPLAY_NONE = { display: 'none' };

export default class Form extends Component {
  static propTypes = {
    autoComplete: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired
  };

  static defaultProps = {
    autoComplete: true
  };

  state = { submitting: false };

  handleSubmit(e) {
    const { onSubmit } = this.props;

    // form submission's default action of going to the URL is always prevented for this form component
    e.preventDefault();
    e.stopPropagation();

    // Call onSubmit callback
    onSubmit(e);
  }

  // public function to trigger a submit on the form
  submit() {
    const { submitting } = this.state;

    if (!submitting) {
      this.setState({ submitting: true }, () => {
        const { _tempSubmitBtn } = this.refs;
        if (_tempSubmitBtn) {
          findDOMNode(_tempSubmitBtn).click();
          this.setState({ submitting: false });
        }
      });
    }
  }

  renderAutocompletePrevention() {
    const { autoComplete } = this.props;

    if (autoComplete) {
      return null;
    }

    return (
      <span key="no-autocomplete">
        <input key="_ac_text" type="text" style={DISPLAY_NONE}/>
        <input key="_ac_pass" type="password" style={DISPLAY_NONE}/>
      </span>
    );
  }

  renderHiddenSubmit() {
    const { submitting } = this.state;

    if (!submitting) {
      return null;
    }

    return (
      <input key="hidden-submit" type="submit" ref="_tempSubmitBtn" style={DISPLAY_NONE}/>
    );
  }

  render() {
    const { children, className, autoComplete } = this.props;

    return (
      <form onSubmit={(e) => this.handleSubmit(e)} {...{ className, autoComplete }}>
        {this.renderAutocompletePrevention()}
        <span key="children">
          {children}
        </span>
        {this.renderHiddenSubmit()}
      </form>
    );
  }
}