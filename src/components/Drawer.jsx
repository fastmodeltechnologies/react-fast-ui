import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from './Button';
import classnames from 'classnames';

const UP_ARROW = 'angle-up';
const DOWN_ARROW = 'angle-down';

export default class Drawer extends Component {
  static propTypes = {
    downArrowIcon: PropTypes.string,
    icon: PropTypes.string,
    menuClasses: PropTypes.string,
    upArrowIcon: PropTypes.string
  };

  static defaultProps = {
    downArrowIcon: DOWN_ARROW,
    icon: null,
    upArrowIcon: UP_ARROW
  };

  getDrawerClasses() {
    const { open } = this.props;
    return classnames(
      { 'display-none': !open },
      'list-unstyled'
    );
  }

  render() {
    const { buttonClasses, children, className, downArrowIcon, icon, onClose, onOpen, open, upArrowIcon } = this.props;

    // props for the button that opens and closes the drawer
    const buttonProps = Object.assign({},
      this.props,
      {
        borderless: true,
        className: buttonClasses || '',
        icon: icon || (open ? upArrowIcon : downArrowIcon)
      }
    );

    return (
      <div className={className}>
        <Button {...buttonProps} onClick={open ? onClose : onOpen}/>
        <ul className={this.getDrawerClasses()}>
          {
            React.Children.map(children,
              (child) => {
                return (
                  <li className='mg-left-sm'>
                    {React.cloneElement(child)}
                  </li>
                );
              }
            )
          }
        </ul>
      </div>
    );
  }
}