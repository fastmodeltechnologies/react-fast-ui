import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

export default class Spinner extends PureComponent {
  render() {
    return (
      <Icon name='refresh' animate='spin' {...this.props}/>
    );
  }
}