import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import cx from 'classnames';
import _ from 'underscore';

const ICON_SIZE_MAP = {
  xs: '',
  sm: '1x',
  md: 'lg',
  lg: '2x',
  xl: '3x'
};

const SIZES = [ 'xs', 'sm', 'md', 'lg', 'xl' ];

export default class IconButton extends Component {
  static propTypes = {
    color: PropTypes.string,
    colorOverride: PropTypes.bool,
    disabled: PropTypes.bool,
    fontSize: PropTypes.oneOf(SIZES),
    icon: PropTypes.string.isRequired,
    iconSize: PropTypes.oneOf(SIZES),
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string,
  };

  static defaultProps = {
    color: 'primary',
    colorOverride: false,
    disabled: false,
    fontSize: 'xs',
    iconSize: 'md',
    text: null
  };

  render() {
    const { className, color, colorOverride, disabled, fontSize, icon, iconSize, onClick, text } = this.props;
    const parentClasses = cx(
      'align-items-center',
      { 'cursor-pointer': !disabled },
      { 'color-light-gray': !colorOverride || disabled },
      'display-flex',
      'flex-direction-column',
      { [ `hover-color-${color}` ]: !disabled },
      'hover-visibility-parent',
      className
    );
    const textClasses = cx(
      `${fontSize}-font`,
      `mg-top-${fontSize}`,
      'visibility-hidden',
      'visibility-child-visible'
    );

    return (
      <div className={parentClasses} onClick={disabled ? null : onClick}>
        <Icon name={icon} size={ICON_SIZE_MAP[ iconSize ]}/>
        {text ? <div className={textClasses}>{text}</div> : null}
      </div>
    );
  }
}
