// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';

export default class Portal extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  constructor(props) {
    super(props);
    this._element = document.createElement('div');
  }


  componentDidMount() {
    const portalRoot = document.getElementById('portal-root');
    portalRoot.appendChild(this._element);
  }


  componentWillUnmount() {
    const portalRoot = document.getElementById('portal-root');
    portalRoot.removeChild(this._element);
  }

  render() {
    return createPortal(
      this.props.children,
      this._element
    );
  }
}