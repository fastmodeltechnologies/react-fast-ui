import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Icon from './Icon';
import _ from 'underscore';

const VALUE_SHAPE = PropTypes.oneOfType([ PropTypes.string, PropTypes.bool, PropTypes.number ]);
const OPTION_SHAPE = PropTypes.shape({
  label: PropTypes.node.isRequired,
  value: VALUE_SHAPE.isRequired
});

const DEMO_OPTIONS = [
  { label: <Icon name="user"/>, value: 'option 1' },
  { label: 2, value: 'option 2' },
  { label: <span className="color-fastmodel-red">Option 3</span>, value: 'option 3' }
];

// common option cx
const COMMON_CLASSNAMES = cx(
  'border-style-solid',
  'border-width-1px',
  'cursor-pointer',
  'display-inline-block',
  'pd-top-sm',
  'pd-bottom-sm',
  'pd-left-md',
  'pd-right-md'
);

// classname for selected option
const SELECTED_CLASSNAME = cx(
  'bg-color-rich-orange',
  'hover-bg-color-rich-orange-darken',
  'color-white',
  'border-color-rich-orange-darken'
);

// classname for non-selected option
const NON_SELECTED_CLASSNAME = cx(
  'bg-color-white',
  'hover-bg-color-lightest-gray',
  'color-light-gray',
  'border-color-light-gray'
);

// classname for first option
const FIRST_CLASSNAME = cx(
  'border-top-left-radius-sm',
  'border-bottom-left-radius-sm'
);

// classname for last option
const LAST_CLASSNAME = cx(
  'border-top-right-radius-sm',
  'border-bottom-right-radius-sm'
);

class Option extends Component {
  static propsTypes = {
    first: PropTypes.bool.isRequired,
    last: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    option: OPTION_SHAPE,
    selected: PropTypes.bool.isRequired
  };

  render() {
    const { first, last, onClick, option, selected } = this.props;
    const className = cx(
      COMMON_CLASSNAMES,
      first ? FIRST_CLASSNAME : null,
      last ? LAST_CLASSNAME : null,
      selected ? SELECTED_CLASSNAME : NON_SELECTED_CLASSNAME
    );

    return (
      <div className={className} onClick={onClick}>
        {option.label}
      </div>
    );
  }
}

export default class ButtonToggle extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(OPTION_SHAPE),
    value: VALUE_SHAPE.isRequired
  };

  render() {
    const { className, onChange, options, value } = this.props;

    return (
      <span className={className}>
        {options.map((opt, i) => {
          return (
            <Option
              key={i}
              first={i == 0}
              last={i == (options.length - 1)}
              onClick={() => onChange(opt.value)}
              option={opt}
              selected={opt.value === value}
            />
          );
        })}
      </span>
    );
  }
}
