import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TableCell extends Component {
  static propTypes = {};

  static defaultProps = {};

  render() {
    const { style, ...rest } = this.props;

    return (
      <td style={{ padding: 8, ...style }} {...rest}/>
    );
  }
}