import React, { Children, cloneElement, Component } from 'react';
import TableContext from "./TableContext";

export default class TableRow extends Component {

  static contextType = TableContext;

  render() {
    const { children, ...rest } = this.props;
    const { gutterClassName } = this.context;

    return (
      <tr {...rest}>
        <td className={gutterClassName}/>
        {children}
        <td className={gutterClassName}/>
      </tr>
    );
  }
}