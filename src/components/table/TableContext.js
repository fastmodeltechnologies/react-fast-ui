import React from 'react';
import Table from '../Table';

const TableContext = React.createContext({});

export default TableContext;