import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TableContext from "./TableContext";

export default class TableHead extends Component {
  static contextType = TableContext;

  render() {
    const { style, children, ...rest } = this.props,
      { maxWidth, gutterClassName } = this.context;

    const maxWidthCellBorders = {
      width: maxWidth !== null ? 22 : 0
    };

    return (
      <thead style={{ borderBottom: 'lightgray 1px solid', ...style }} {...rest}>
      <tr>
        <th style={maxWidthCellBorders} className={gutterClassName}/>
        {children}
        <th style={maxWidthCellBorders} className={gutterClassName}/>
      </tr>
      </thead>
    );
  }
}