import React, { Children, cloneElement, Component } from 'react';
import PropTypes from 'prop-types';

export default class TableBody extends Component {
  static propTypes = {
    striped: PropTypes.bool,
  };

  static defaultProps = {
    striped: true
  };

  render() {
    const { children, striped, ...rest } = this.props;

    return (
      <tbody {...rest}>
      {
        Children.map(children, (child, index) => {
          const odd = index % 2 === 0,
            first = index === 0;

          const { style } = child.props;

          return cloneElement(
            child,
            {
              style: {
                backgroundColor: striped && odd ? '#f9f9f9' : 'white',
                borderTop: first ? null : '1px solid #c4c4c4',
                ...style
              }
            }
          );
        })
      }
      </tbody>
    );
  }
}