import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default class HeaderCell extends Component {
  static propTypes = {
    onSort: PropTypes.func,
    sorted: PropTypes.oneOf([ 'A', 'D' ])
  };

  static defaultProps = {
    sorted: null,
    onSort: null
  };

  /**
   * Helper function for getting the new sort info based on the old sort info and the sorted attribute
   * @param oldSortInfo old sort info object
   * @param attribute attribute sorted on
   * @returns {{attribute: *, desc: boolean}}
   */
  static getNewSortInfo(oldSortInfo, attribute) {
    return {
      attribute,
      desc: (oldSortInfo && oldSortInfo.attribute === attribute ? !oldSortInfo.desc : true)
    };
  }

  /**
   * Helper function that returns the sorted and onSort attributes based on the sortInfo and the attribute name
   * @param sortInfo
   * @param onSortAttribute
   * @param attribute
   * @returns {{sorted: *, onSort: *}}
   */
  static sortAttributes(sortInfo = null, onSortAttribute = null, attribute) {
    let sorted = null;
    if (sortInfo !== null) {
      if (sortInfo.attribute === attribute) {
        sorted = sortInfo.desc ? 'D' : 'A';
      }
    }

    let onSort = null;

    if (onSortAttribute !== null) {
      onSort = () => onSortAttribute(HeaderCell.getNewSortInfo(sortInfo, attribute));
    }

    return { sorted, onSort };
  }

  handleClick = e => {
    const { onSort } = this.props;
    if (onSort !== null) {
      onSort();
    }
  };

  render() {
    const { style, children, sorted, onSort, ...rest } = this.props;

    return (
      <th style={{
        cursor: onSort !== null ? 'pointer' : null,
        borderBottom: sorted ? '3px solid #e65d1d' : null,
        padding: 8,
        verticalAlign: 'bottom',
        ...style
      }} {...rest} onClick={this.handleClick}>
        <div className="display-flex align-items-center">
          <div className="flex-grow-1">
            {children}
          </div>

          <div className="flex-shrink-0">
            {
              sorted === null ? null :
                <Icon name={sorted === 'D' ? 'angle-down' : 'angle-up'} className="mg-left-sm"/>
            }
          </div>
        </div>
      </th>
    );
  }
}