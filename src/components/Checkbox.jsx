import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Icon from './Icon';

export default class Checkbox extends PureComponent {
  static propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    size: PropTypes.oneOf(Icon.sizes),
  };

  static defaultProps = {
    checked: false,
    disabled: false,
  };

  handleClick = () => {
    const { onChange, checked, disabled } = this.props;

    if (!disabled) {
      onChange(!checked);
    }
  };

  render() {
    const { checked, children, size, disabled, style, className } = this.props;
    const icon = checked ? 'check-square-o' : 'square-o';

    const CX = cx(
      { 'color-primary': checked },
      { 'color-light-gray cursor-not-allowed': disabled },
      { 'cursor-pointer': !disabled },
      className
    );

    return (
      <div onClick={this.handleClick} className={CX} style={style}>
        <Icon name={icon} size={size}/>
        {children}
      </div>
    );
  };
}