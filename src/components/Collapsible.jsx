import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import controllable from 'react-controllables';
import Icon from './Icon';

class Collapsible extends Component {
  static propTypes = {
    alwaysRenderChildren: PropTypes.bool,
    label: PropTypes.node.isRequired,
    collapsed: PropTypes.bool.isRequired,
    onCollapsedChange: PropTypes.func.isRequired,
    upIcon: PropTypes.node,
    downIcon: PropTypes.node,
    summary: PropTypes.node,
    summaryClassName: PropTypes.string,
    style: PropTypes.object,
    collapsedStyle: PropTypes.object,
    expandedStyle: PropTypes.object
  };

  static defaultProps = {
    alwaysRenderChildren: false,
    upIcon: <Icon name="angle-up"/>,
    downIcon: <Icon name="angle-down"/>,
    summary: null,
    summaryClassName: null
  };

  renderChildren() {
    const { alwaysRenderChildren, children, collapsed } = this.props;

    if (alwaysRenderChildren) {
      const styles = collapsed ? { height: 0, visibility: 'hidden' } : {};
      return <div style={styles}>{children}</div>;
    } else {
      return (collapsed) ? null : children;
    }
  }

  render() {
    const {
      label, summary, collapsed, onCollapsedChange, className, upIcon, downIcon, summaryClassName,
      style, collapsedStyle, expandedStyle
    } = this.props;
    const calculatedStyle = Object.assign({}, style, collapsed ? collapsedStyle : expandedStyle);

    return (
      <div className={cx(className)} style={calculatedStyle}>
        <div key="summary" onClick={() => onCollapsedChange(!collapsed)}
             className={cx('display-flex align-items-center cursor-pointer pd-sm', summaryClassName)}>
          <label className="mg-none">
            {label}
          </label>
          <div
            className="flex-grow-1 flex-shrink-1 mg-left-sm mg-right-sm text-nowrap text-overflow-ellipsis overflow-hidden">
            <em>{collapsed ? summary : null}</em>
          </div>
          <div>
            {collapsed ? downIcon : upIcon}
          </div>
        </div>
        {this.renderChildren()}
      </div>
    );
  }
}

export default controllable(Collapsible, [ 'collapsed' ]);