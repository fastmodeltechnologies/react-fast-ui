import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const BUTTON_SIZES = [ 'xs', 'sm', 'md', 'lg', 'xl' ];
const BUTTON_TYPES = [ 'primary', 'secondary', 'danger', 'success', 'warning' ];

const getBgColorClasses = (type: string) => `bg-color-${type} hover-bg-color-${type}-darken`;
const getBorderClasses = (type: string) => (
  `border-width-1px border-style-solid border-radius-xxl border-color-${type}-darken`
);

export default class SimpleButton extends PureComponent {
  static sizes = BUTTON_SIZES;
  static types = BUTTON_TYPES;

  static propTypes = {
    disabled: PropTypes.bool,
    href: PropTypes.string,
    onClick: PropTypes.func,
    size: PropTypes.oneOf(BUTTON_SIZES),
    tabIndex: PropTypes.string,
    type: PropTypes.oneOf(BUTTON_TYPES),
  };

  static defaultProps = {
    disabled: false,
    href: (null: ?string),
    onClick: (null: ?Function),
    size: 'md',
    tabIndex: (null: ?string),
    type: 'primary',
  };

  beforeOnClick = (e: SyntheticEvent) => (this.props.onClick ? this.props.onClick(e) : null);

  focus = () => this.refs.button.focus();

  getPaddingClasses = () => {
    const { size } = this.props;
    const verticalPadding = size === 'sm' ? 'xs' : 'sm';

    return `pd-top-${verticalPadding} pd-right-${size} pd-bottom-${verticalPadding} pd-left-${size}`;
  };

  render() {
    const { children, className, disabled, href, size, style, tabIndex, type, ...rest } = this.props;
    const classNames = cx(
      'color-white hover-color-white',
      (disabled ? 'bg-color-light-gray bg-hover-color-light-gray' : getBgColorClasses(type)),
      (disabled ? 'cursor-not-allowed' : 'cursor-pointer'),
      `${size}-font`,
      getBorderClasses(type),
      this.getPaddingClasses(),
      className
    );
    const styles = { textDecoration: 'none', ...style };
    const onClick = (e) => (disabled ? null : this.beforeOnClick(e));

    return (
      <a
        {...rest}
        className={classNames}
        style={styles}
        ref="button"
        href={href}
        onClick={onClick}
        tabIndex={tabIndex}>
        {children}
      </a>
    );
  }
}