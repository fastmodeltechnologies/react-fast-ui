import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

const ALERT_TYPES = [ 'success', 'info', 'danger', 'warning' ];

export default class Alert extends Component {
  static propTypes = {
    level: PropTypes.oneOf(ALERT_TYPES).isRequired,
    strong: PropTypes.node,
    icon: PropTypes.node,
    message: PropTypes.node.isRequired
  };

  static defaultProps = {
    level: 'info',
    strong: null,
    icon: null
  };

  static types = ALERT_TYPES;

  getIcon() {
    let { icon } = this.props;

    if (icon === null) {
      return null;
    }

    if (typeof icon === 'string') {
      icon = (<Icon name={icon}/>);
    }

    return <span className='Alert--icon mg-right-sm'>{icon}</span>;
  }

  getStrong() {
    const { strong } = this.props;

    if (strong === null) {
      return null;
    }

    return (
      <strong key='strong' className='Alert-strong'>
        {strong}
      </strong>
    );
  }

  render() {
    const { level, message } = this.props;

    return (
      <div role='Alert' className={`Alert Alert-${level}`}>
        {this.getIcon()}
        {this.getStrong()}
        <span key='text' className='Alert-text mg-left-sm'>
          {message}
        </span>
      </div>
    );
  }
}