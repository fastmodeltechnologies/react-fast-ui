import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Spinner from './Spinner';

const FILL_RELATIVE_CONTAINER = { position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 };
const LOADING_SCRIM = (
  <div style={{ ...FILL_RELATIVE_CONTAINER, opacity: 0.8, backgroundColor: 'white' }}/>
);

const MID_SPINNER = (
  <div style={FILL_RELATIVE_CONTAINER}
       className="display-flex align-items-center justify-content-center">
    <Spinner/>
  </div>
);

export default class LoadingWrapper extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    loadingScrim: PropTypes.node,
    spinner: PropTypes.node
  };

  static defaultProps = {
    loadingScrim: LOADING_SCRIM,
    spinner: MID_SPINNER
  };

  render() {
    const { loading, loadingScrim, spinner, style, children, ...rest } = this.props;

    return (
      <div style={{ ...style, position: 'relative' }} {...rest}>
        {children}

        {
          loading ? (
            <span>
              {loadingScrim}
              {spinner}
            </span>
          ) : null
        }
      </div>
    );
  }
}