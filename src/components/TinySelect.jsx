import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import cx from 'classnames';
import OnClickOut from './OnClickOut';

const MENU_CLASSES = 'box-shadow-sm mg-top-sm';

const MENU_STYLE = {
  position: 'absolute',
  textAlign: 'center',
  color: 'black',
  backgroundColor: 'white',
  top: '100%',
  left: '50%',
  zIndex: 1,
  transform: 'translateX(-50%)',
  overflow: 'auto',
  maxHeight: 400
};

const CARET = <Icon name="angle-down" style={{ marginLeft: 4 }}/>;

class Value extends PureComponent {
  static propTypes = {
    option: PropTypes.object.isRequired,
    labelKey: PropTypes.string.isRequired
  };

  render() {
    const { option, labelKey, ...rest } = this.props;

    return (
      <span {...rest}>
        {option[ labelKey ]}
      </span>
    );
  }
}

export default class TinySelect extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.object).isRequired,
    value: PropTypes.any,
    labelKey: PropTypes.string,
    valueKey: PropTypes.string,
    showSelected: PropTypes.bool,
    placeholder: PropTypes.node
  };

  static defaultProps = {
    placeholder: <em>Select...</em>,
    labelKey: 'label',
    valueKey: 'value',
    showSelected: true
  };

  state = {
    open: false
  };

  handleChange = (option) => {
    const { onChange } = this.props;
    this.setState({ open: false }, () => onChange(option));
  };

  handleClickOut = () => {
    this.setState({ open: false });
  };

  handleClickValue = () => {
    const { open } = this.state;
    this.setState({ open: !open });
  };

  renderOptions() {
    const { options, labelKey, valueKey, showSelected, value } = this.props;
    const { open } = this.state;

    if (!open) {
      return null;
    }

    return (
      <div className={MENU_CLASSES} style={MENU_STYLE}>
        {
          options.map((option, index) => {
            const isSelected = value && option[ valueKey ] == value[ valueKey ];

            if (isSelected && !showSelected) {
              return null;
            }

            const className = cx(
              'pd-sm text-nowrap bg-color-white hover-bg-color-lightest-gray',
              { 'border-bottom-color-gray-background-2px': index < options.length - 1 }
            );

            return (
              <div className={className} key={option[ valueKey ]} onClick={() => this.handleChange(option)}>
                <Value labelKey={labelKey} option={option}/>
              </div>
            );
          })
        }
      </div>
    );
  }

  render() {
    const { className, style, placeholder, value, labelKey } = this.props;

    return (
      <OnClickOut onClickOut={this.handleClickOut}>
        <span style={style} className={cx('cursor-pointer position-relative', className)}>
          <span onClick={this.handleClickValue} className="text-nowrap">
            {value ? <Value labelKey={labelKey} option={value}/> : placeholder}
            {CARET}
          </span>
          {this.renderOptions()}
        </span>
      </OnClickOut>
    );
  }
}
