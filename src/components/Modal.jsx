import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import cx from 'classnames';
import Portal from './Portal';
import Icon from './Icon';
import FixedPage from 'react-fixed-page-ms';

const ESCAPE_KEYCODE = 27;

const ModalBody = ({ className, ...rest }) => (
  <section className={cx('pd-md', className)} {...rest}/>
);

const ModalFooter = ({ className, children, ...rest }) => (
  <footer className={cx('display-flex justify-content-flex-end pd-md', className)} {...rest}>
    <div className="flex-shrink-0">{children}</div>
  </footer>
);

const CloseButton = ({ closeButton, ...rest }) => (
  <div className="flex-shrink-0 cursor-pointer color-light-gray hover-color-fastmodel-gray" {...rest}>
    {closeButton || <Icon name="times"/>}
  </div>
);

const ModalTitle = ({ className, onClose, children, style, ...rest }) => (
  <header className={cx('pd-md display-flex flex-direction-row align-items-center', className)}
          style={{ borderBottom: '1px solid rgba(0,0,0,0.2)', ...style }}>
    <h4 style={{ margin: 0 }} className="flex-grow-1">{children}</h4>
    {onClose != null ? <CloseButton onClick={() => onClose()}/> : null}
  </header>
);

const ModalTransition = (props) => <TransitionGroup  {...props}/>;

export default class Modal extends PureComponent {
  static Title = ModalTitle;
  static Body = ModalBody;
  static Footer = ModalFooter;

  static propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    backdrop: PropTypes.oneOf([ true, false, 'static', 'full' ]),
    size: PropTypes.oneOf([ 'xs', 'sm', 'lg', 'full' ]),
    // the level of stacking to render at
    aside: PropTypes.oneOf([ false, 1, 2, 3, 4, 5 ]),
    style: PropTypes.object,
    closeButtonStyle: PropTypes.object,
    closeButton: PropTypes.node,
    contentContainerStyle: PropTypes.object,
  };

  static defaultProps = {
    backdrop: 'static',
    size: 'sm',
    aside: false
  };

  componentDidUpdate(prevProps) {
    const { open } = this.props;

    if (open && !prevProps.open) {
      // we need a timeout because this should happen *after* the portal renders it to the DOM
      setTimeout(this.focusModalSection, 0);
    }
  }

  focusModalSection = () => {
    const { section } = this.refs;
    if (section) {
      section.focus();
    }
  };

  handleBackdropClick = () => {
    const { backdrop, onClose } = this.props;

    if (backdrop !== 'static' && backdrop !== 'full') {
      onClose();
    }
  };

  handleKeyDown = ({ keyCode }) => {
    const { onClose } = this.props;

    if (keyCode === ESCAPE_KEYCODE) {
      onClose();
    }
  };

  renderModalSection() {
    const {
      open,
      onClose,
      backdrop,
      size,
      aside,
      children,
      style,
      closeButton,
      closeButtonStyle,
      contentContainerStyle
    } = this.props;

    if (!open) {
      return null;
    }

    let backdropEl;

    if (!backdrop) {
      backdropEl = null;
    } else if (backdrop === 'full') {
      backdropEl = (
        <div
          key="backdrop"
          style={{ backgroundColor: '#fff' }}
          className='Modal--backdrop'
          onClick={this.handleBackdropClick}/>
      );
    } else {
      backdropEl = <div key="backdrop" className='Modal--backdrop' onClick={this.handleBackdropClick}/>;
    }

    return (
      <CSSTransition key='modal' classNames='Modal' timeout={{ enter: 300, exit: 300 }}>
        <section className='Modal--container' ref='section' onKeyDown={this.handleKeyDown} tabIndex={-1}>

          {backdropEl}
          {backdrop === 'full' &&
          <CloseButton
            closeButton={closeButton}
            style={{ position: 'absolute', top: 20, right: 20, ...closeButtonStyle }}
            onClick={onClose}/>
          }

          <div
            style={style}
            className={cx('display-flex justify-content-center', { [ `Modal--dialog-aside aside-${aside}` ]: aside != false }, { 'full-height': size === 'full' })}>
            <div className={`Modal--content ${size}`} style={contentContainerStyle}>
              {children}
            </div>
          </div>

        </section>
      </CSSTransition>
    );
  }

  render() {
    return (
      <span>
        <Portal>
          <ModalTransition>
            {this.renderModalSection()}
          </ModalTransition>
        </Portal>
        <FixedPage fixed={this.props.open}/>
      </span>
    );
  }
}