import React, { Children, cloneElement, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import getScrollParent from 'scrollparent';
import _ from 'underscore';
import Portal from './Portal';

// this function returns a number within some bounds
const bound = (num, min, max) => Math.max(Math.min(num, max), min);

const BASE_ARROW_STYLES = {
  width: 0,
  height: 0,
  borderStyle: 'solid',
  borderTopColor: 'transparent',
  borderBottomColor: 'transparent',
  borderLeftColor: 'transparent',
  borderRightColor: 'transparent'
};

const EMPTY_PLACEMENT_INFO = {
  popoverStyle: null,
  arrowStyle: null
};

const PLACEMENT_TOP = 'top';
const PLACEMENT_RIGHT = 'right';
const PLACEMENT_BOTTOM = 'bottom';
const PLACEMENT_LEFT = 'left';
const PLACEMENTS = [ PLACEMENT_TOP, PLACEMENT_RIGHT, PLACEMENT_BOTTOM, PLACEMENT_LEFT ];

/**
 * This element is used to show content surrounding some element, such as a tooltip or additional action buttons
 */
export default class Popover extends PureComponent {
  static PLACEMENTS = PLACEMENTS;

  static propTypes = {
    open: PropTypes.bool.isRequired,
    content: PropTypes.oneOfType([ PropTypes.arrayOf(PropTypes.node), PropTypes.node.isRequired ]).isRequired,
    buffer: PropTypes.number,
    arrowSize: PropTypes.number,

    arrowClassName: PropTypes.string,
    arrowStyle: PropTypes.object,
    arrowColor: PropTypes.string,
    popoverClassName: PropTypes.string,
    popoverStyle: PropTypes.object,
    defaultPlacement: PropTypes.oneOf([ PLACEMENT_TOP, PLACEMENT_RIGHT, PLACEMENT_BOTTOM, PLACEMENT_LEFT ])
  };

  static defaultProps = {
    buffer: 8,
    arrowSize: 6,
    arrowColor: 'white',

    popoverStyle: { borderRadius: 4, boxShadow: '0 2px 5px 2px #c4c4c4', backgroundColor: 'white' },
    popoverClassName: null,
    arrowStyle: null,
    arrowClassName: null,
    defaultPlacement: PLACEMENT_TOP,
    style: { zIndex: 100 }
  };

  // in the state we store information used in the positioning of the popover
  state = {
    positionData: null
  };

  // this element will try to wrap a single child if that's all it gets
  getChildNode() {
    const { _children } = this;
    if (_children) {
      return findDOMNode(_children);
    }

    return null;
  }

  /**
   * Copy information about the bounding rectangle of the child and the popover content dimensions into the state
   */
  setClientRects = () => {
    const { content } = this.refs,
      children = this.getChildNode(),
      { positionData } = this.state;

    if (children && content && window) {
      const { top: childTop, right: childRight, bottom: childBottom, left: childLeft } = children.getBoundingClientRect(),
        { width: contentWidth, height: contentHeight } = content.getBoundingClientRect(),
        { innerHeight: windowHeight, innerWidth: windowWidth } = window;

      const newPositionData = {
        childTop,
        childRight,
        childBottom,
        childLeft,
        contentWidth,
        contentHeight,
        windowHeight,
        windowWidth
      };

      if (!_.isEqual(positionData, newPositionData)) {
        this.setState({ positionData: newPositionData }, this.setClientRects);
      }
    }
  };

  _throttledSetClientRect = _.throttle(() => {
    const { open } = this.props;
    if (open) {
      this.setClientRects();
    }
  }, 30);

  // Bind event listeners to calculate a new position of the popover
  componentDidMount() {
    window.addEventListener('scroll', this._throttledSetClientRect);
    window.addEventListener('resize', this._throttledSetClientRect);

    this._scrollParent = getScrollParent(this.getChildNode());
    this._scrollParent.addEventListener('scroll', this._throttledSetClientRect);

    this._throttledSetClientRect();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this._throttledSetClientRect);
    window.removeEventListener('resize', this._throttledSetClientRect);

    this._scrollParent.removeEventListener('scroll', this._throttledSetClientRect);
  }

  /**
   * Check to see that the scroll parent listener is correct
   */
  updateScrollParentListener() {
    const scrollParent = getScrollParent(this.getChildNode());

    if (this._scrollParent !== scrollParent) {
      this._scrollParent.removeEventListener('scroll', this._throttledSetClientRect);
      scrollParent.addEventListener('scroll', this._throttledSetClientRect);
      this._scrollParent = scrollParent;
    }
  }

  componentDidUpdate(prevProps) {
    this.updateScrollParentListener();

    const { open } = this.props;
    const { open: prevOpen } = prevProps;

    // If popover has just opened, update client rects after a short delay to ensure
    // the content ref element has an accurate width.
    if (open && !prevOpen) {
      window.setTimeout(() => this.setClientRects(), 100);
    }
  }

  /**
   * Calculate the placement style of the fixed popover container
   * @returns {*}
   */
  getPlacementStyle() {
    const { positionData } = this.state,
      { buffer, arrowSize, arrowColor, defaultPlacement } = this.props;

    if (positionData === null) {
      return EMPTY_PLACEMENT_INFO;
    }

    const {
      contentHeight, contentWidth,
      windowHeight, windowWidth,
      childTop, childRight, childBottom, childLeft
    } = positionData;

    // the dimensions of the child
    const childWidth = childRight - childLeft,
      childHeight = childBottom - childTop;

    // calculate whether it fits in each of the four different placements
    const fitsIn = {
      [ PLACEMENT_TOP ]: (childTop >= contentHeight + childHeight + buffer + arrowSize && childLeft >= buffer && childRight <= windowWidth - buffer),
      [ PLACEMENT_RIGHT ]: (childRight + contentWidth + buffer + arrowSize <= windowWidth && childTop >= buffer && childBottom <= windowHeight - buffer),
      [ PLACEMENT_BOTTOM ]: (childBottom + contentHeight + buffer + arrowSize <= windowHeight && childLeft >= buffer && childRight <= windowWidth - buffer),
      [ PLACEMENT_LEFT ]: (childLeft - contentWidth - buffer - arrowSize >= 0 && childTop >= buffer && childBottom <= windowHeight - buffer)
    };

    let placement = defaultPlacement;

    if (!fitsIn[ defaultPlacement ]) {
      placement = _.chain(fitsIn).omit(val => !val).keys().first().value() || defaultPlacement;
    }

    switch (placement) {
      case PLACEMENT_TOP:
        return {
          popoverPlacement: {
            top: childTop - contentHeight - arrowSize,
            left: bound(childLeft + childWidth / 2 - contentWidth / 2, buffer, windowWidth - buffer - contentWidth)
          },

          arrowPlacement: {
            top: childTop - arrowSize,
            left: childLeft + childWidth / 2 - arrowSize,
            borderWidth: arrowSize,
            borderTopColor: arrowColor
          }
        };
      case PLACEMENT_RIGHT:
        return {
          popoverPlacement: {
            top: bound(childTop + childHeight / 2 - contentHeight / 2, buffer, windowHeight - buffer - contentHeight),
            left: childRight + arrowSize
          },

          arrowPlacement: {
            left: childRight - arrowSize,
            top: childTop + childHeight / 2 - arrowSize,
            borderWidth: arrowSize,
            borderRightColor: arrowColor
          }
        };
      case PLACEMENT_BOTTOM:
        return {
          popoverPlacement: {
            top: childBottom + arrowSize,
            left: bound(childLeft + childWidth / 2 - contentWidth / 2, buffer, windowWidth - buffer - contentWidth)
          },

          arrowPlacement: {
            left: childLeft + childWidth / 2 - arrowSize,
            top: childTop + childHeight - arrowSize,
            borderWidth: arrowSize,
            borderBottomColor: arrowColor
          }
        };
      case PLACEMENT_LEFT:
        return {
          popoverPlacement: {
            top: bound(childTop + childHeight / 2 - contentHeight / 2, buffer, windowHeight - buffer - contentHeight),
            left: childLeft - contentWidth - arrowSize
          },

          arrowPlacement: {
            top: childTop + childHeight / 2 - arrowSize,
            left: childLeft - arrowSize,
            borderWidth: arrowSize,
            borderLeftColor: arrowColor
          }
        };
      default:
        return EMPTY_PLACEMENT_INFO;
    }
  }

  setChildRef = child => this._children = child;

  getChildren() {
    const { children } = this.props;

    if (Children.count(children) === 1) {
      return cloneElement(children, { ref: this.setChildRef });
    }

    return (
      <div ref={this.setChildRef} style={{ display: 'inline-block' }}>
        {children}
      </div>
    );
  }

  render() {
    const { content, open, arrowClassName, popoverClassName, style, arrowStyle, popoverStyle } = this.props;
    const { popoverPlacement, arrowPlacement } = this.getPlacementStyle();

    return (
      <span>
        {this.getChildren()}

        <Portal>
          <div>
            <div ref="content" className={popoverClassName}
                 style={{
                   position: 'fixed',
                   display: 'inline-block',
                   visibility: !open ? 'hidden' : null,
                   ...style,
                   ...popoverPlacement,
                   ...popoverStyle,
                 }}>
              {content}
            </div>
            <div ref="arrow"
                 className={arrowClassName}
                 style={{
                   position: 'fixed',
                   visibility: !open ? 'hidden' : null,
                   ...style,
                   ...BASE_ARROW_STYLES,
                   ...arrowPlacement,
                   ...arrowStyle,
                 }}/>
          </div>
        </Portal>
      </span>
    );
  }
}