import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Portal from './Portal';
import FixedPage from 'react-fixed-page-ms';

const SidebarTransition = (props: Object) => <TransitionGroup {...props} />;

export default class Sidebar extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    backdrop: PropTypes.bool
  };

  static defaultProps = {
    backdrop: true
  };

  renderSidebar() {
    const { open, children, onClose, backdrop, ...rest } = this.props;

    if (!open) {
      return null;
    }

    return (
      <CSSTransition key='sidebar' classNames='sidebar' timeout={{ enter: 400, exit: 500 }}>
        <div className="sidebar">
          {backdrop ? <div className="sidebar-backdrop" onClick={onClose}/> : null}
          <div className="sidebar-content" {...rest}>{children}</div>
        </div>
      </CSSTransition>
    );
  }

  render() {
    return (
      <span>
        <Portal>
          <SidebarTransition>
            {this.renderSidebar()}
          </SidebarTransition>
        </Portal>
        <FixedPage fixed={this.props.open}/>
      </span>
    );
  }
}