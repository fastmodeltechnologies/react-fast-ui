// @flow
import React, { cloneElement, Children, PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import SimpleButton from './SimpleButton';

const getColorClasses = (type: string) => `color-${type} hover-color-${type}-darken`;

export default class LinkButton extends PureComponent {
  static sizes = SimpleButton.sizes;
  static types = SimpleButton.types;

  static propTypes = {
    disabled: PropTypes.bool,
    href: PropTypes.string,
    onClick: PropTypes.func,
    size: PropTypes.oneOf(LinkButton.sizes),
    tabIndex: PropTypes.string,
    to: PropTypes.string,
    type: PropTypes.oneOf(LinkButton.types),
  };

  static defaultProps = {
    disabled: false,
    href: (null: ?string),
    onClick: (null: ?Function),
    size: 'lg',
    tabIndex: (null: ?string),
    to: (null: ?string),
    type: 'primary',
  };

  beforeOnClick = (e: SyntheticEvent) => (this.props.onClick && !this.props.disabled ? this.props.onClick(e) : null);

  focus = () => this.refs.button.focus();

  render() {
    const { children, className, disabled, href, size, style, type } = this.props;
    const classNames = cx(
      (disabled ? 'color-light-gray hover-color-light-gray' : getColorClasses(type)),
      `${size}-font`,
      className
    );
    const styles = { textDecoration: 'none', cursor: (disabled ? 'not-allowed' : 'pointer'), ...style };

    return (
      <span
        className={classNames}
        style={styles}
        ref="button"
        onClick={this.beforeOnClick}>
        {children}
      </span>
    );
  }
}