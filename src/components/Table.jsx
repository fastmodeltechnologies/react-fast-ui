import React, { Children, cloneElement, Component } from 'react';
import PropTypes from 'prop-types';
import HeaderCell from './table/HeaderCell';
import TableCell from './table/TableCell';
import Head from './table/TableHead';
import Body from './table/TableBody';
import Row from './table/TableRow';
import Dimensions from 'react-dimensions';
import TableContext from './table/TableContext';

class Table extends Component {
  static propTypes = {
    containerWidth: PropTypes.number.isRequired,
    containerHeight: PropTypes.number.isRequired,
    maxWidth: PropTypes.number,

    responsive: PropTypes.bool,
    gutterClassName: PropTypes.string,
    wrapperStyle: PropTypes.object,
  };

  static defaultProps = {
    maxWidth: 1400,
    responsive: true,
    gutterClassName: 'hidden-print',
  };

  constructor(props) {
    super(props);

    this.state = {
      contextValues: {
        containerWidth: props.containerWidth,
        maxWidth: props.maxWidth,
        gutterClassName: props.gutterClassName,
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { containerWidth: prevWidth, maxWidth: prevMax, gutterClassName: prevGutter } = this.props;
    const { containerWidth, maxWidth, gutterClassName } = nextProps;

    if (containerWidth !== prevWidth || maxWidth !== prevMax || gutterClassName !== prevGutter) {
      this.setState({ contextValues: { containerWidth, maxWidth, gutterClassName } });
    }
  }

  render() {
    const { style, wrapperStyle, responsive, containerWidth, maxWidth, gutterClassName, ...rest } = this.props;

    const table = (
      <TableContext.Provider value={this.state.contextValues}>
        <table style={{ width: '100%', ...style }} {...rest}/>
      </TableContext.Provider>
    );

    if (!responsive) {
      return table;
    }

    return (
      <div style={{ overflow: 'auto', width: '100%', WebkitOverflowScrolling: 'touch', ...wrapperStyle }}>
        {table}
      </div>
    );
  }
}

export default Object.assign(
  Dimensions()(Table),
  {
    demo: Table.demo,
    thead: Head,
    td: TableCell,
    th: HeaderCell,
    tbody: Body,
    tr: Row
  }
);
