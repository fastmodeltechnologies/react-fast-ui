// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import TeamLogo from './TeamLogo';

const HEADER_CLASSES = cx(
  'display-flex justify-content-space-between',
  'flex-shrink-0 full-height'
);

const getGradientString = primaryColor => {
  return `linear-gradient(to right, #${primaryColor}, #${primaryColor}80)`;
};

export default class TeamHeader extends Component {
  static contextTypes = {
    contentWidth: PropTypes.number,
  };

  static propTypes = {
    primaryColor: PropTypes.string,
    fullName: PropTypes.string,
    logo: PropTypes.string,
    showConferenceRecord: PropTypes.bool,
    // Insert Component to show conference record
    teamRecord: PropTypes.any,
  };

  static defaultProps = {
    showConferenceRecord: false,
    primaryColor: '#000000',
    fullName: '',
    logo: false,
  };

  // lifecycle
  render() {
    const { className, children, style, primaryColor, fullName, logo, teamRecord, showConferenceRecord } = this.props;

    const headerStyle = { height: 132, width: '100%', backgroundColor: '#000', top: 0, zIndex: -1 };

    return (
      <header style={{ position: 'relative', height: 132 }}>
        <div
          className="hidden-print position-absolute"
          style={{
            ...headerStyle,
            backgroundImage: getGradientString(primaryColor),
          }}/>
        <div className={className} style={{ height: 132, ...style }}>
          <section style={{ margin: 'auto', paddingLeft: 44, paddingRight: 44 }} className={HEADER_CLASSES}>
            <div className="display-flex flex-direction-column justify-content-flex-end full-height">
              <h1
                className="mg-none whitespace-nowrap print-color-white futura-pt-bold text-transform-uppercase"
                style={{ marginTop: 0, marginBottom: 0, fontSize: 50 }}>
                {fullName}
                {showConferenceRecord &&
                <span className="futura-pt-md" style={{ fontSize: 22, paddingLeft: 10 }}>
                  {teamRecord}
                </span>}
              </h1>
              {children}
            </div>

            <div className="display-flex flex-direction-column justify-content-center full-height">
              <div style={{ backgroundImage: `url(${logo}` }}/>
              <TeamLogo url={logo} height={100}/>
            </div>
          </section>
        </div>
      </header>
    );
  }
}