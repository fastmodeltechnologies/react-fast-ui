import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export default class Circle extends Component {
  static propTypes = {
    diameter: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired
  };

  render() {
    const { diameter, style, children, className, ...rest } = this.props;
    const classes = cx(
      'mask',
      'border-radius-100',
      'display-flex',
      'align-items-center',
      'justify-content-center',
      'overflow-hidden',
      className
    );

    return (
      <div className={classes} style={Object.assign({ width: diameter, height: diameter }, style)} {...rest}>
        {children}
      </div>
    );
  }
}
