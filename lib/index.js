'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COMPONENTS = [
// A
'Alert',

// B
'Button', 'ButtonToggle',

// C
'Circle', 'Collapsible', 'CornerAngle', 'Checkbox',

// D
'Drawer',

// F
'Form',

// I
'Icon', 'IconButton', 'InverseButton',

// L
'LightBox', 'LinkButton', 'LoadingWrapper',

// M
'Modal',

// O
'OnClickOut',

// P
'PageHeader', 'PageTitle', 'Popover', 'Portal',

// S
'Spinner', 'Sidebar', 'SimpleButton',

// T
'Table', 'Tabs', 'TinySelect', 'Tip', 'TeamLogo', 'TeamHeader'];

var DEMOS = {};
var COMPONENTS_WITHOUT_DEMOS = {};

_underscore2.default.forEach(COMPONENTS, function (componentName) {
  var Component = require('./components/' + componentName).default;

  if (Component.demo) {
    // Add demo to DEMOS object
    DEMOS[componentName] = Component.demo;

    // Delete demo from Component that will be exported
    delete Component.demo;
  }

  // Add Component (sans demo) to COMPONENTS_WITHOUT_DEMOS object
  COMPONENTS_WITHOUT_DEMOS[componentName] = Component;
});

module.exports = _extends({
  DEMOS: DEMOS
}, COMPONENTS_WITHOUT_DEMOS);