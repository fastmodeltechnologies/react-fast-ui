'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ALERT_TYPES = ['success', 'info', 'danger', 'warning'];

var Alert = function (_Component) {
  _inherits(Alert, _Component);

  function Alert() {
    _classCallCheck(this, Alert);

    return _possibleConstructorReturn(this, (Alert.__proto__ || Object.getPrototypeOf(Alert)).apply(this, arguments));
  }

  _createClass(Alert, [{
    key: 'getIcon',
    value: function getIcon() {
      var icon = this.props.icon;


      if (icon === null) {
        return null;
      }

      if (typeof icon === 'string') {
        icon = _react2.default.createElement(_Icon2.default, { name: icon });
      }

      return _react2.default.createElement(
        'span',
        { className: 'Alert--icon mg-right-sm' },
        icon
      );
    }
  }, {
    key: 'getStrong',
    value: function getStrong() {
      var strong = this.props.strong;


      if (strong === null) {
        return null;
      }

      return _react2.default.createElement(
        'strong',
        { key: 'strong', className: 'Alert-strong' },
        strong
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          level = _props.level,
          message = _props.message;


      return _react2.default.createElement(
        'div',
        { role: 'Alert', className: 'Alert Alert-' + level },
        this.getIcon(),
        this.getStrong(),
        _react2.default.createElement(
          'span',
          { key: 'text', className: 'Alert-text mg-left-sm' },
          message
        )
      );
    }
  }]);

  return Alert;
}(_react.Component);

Alert.propTypes = {
  level: _propTypes2.default.oneOf(ALERT_TYPES).isRequired,
  strong: _propTypes2.default.node,
  icon: _propTypes2.default.node,
  message: _propTypes2.default.node.isRequired
};
Alert.defaultProps = {
  level: 'info',
  strong: null,
  icon: null
};
Alert.types = ALERT_TYPES;
exports.default = Alert;