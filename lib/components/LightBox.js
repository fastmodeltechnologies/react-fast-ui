'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DEMO_SRC = 'http://media.gettyimages.com/photos/dee-brown-deron-williams-and-luther-head-of-the-illinois-fighting-picture-id165540247'; //eslint-disable-line

var BACKGROUND_CLASSES = (0, _classnames2.default)('position-fixed', 'cursor-pointer', 'left-none', 'z-index-modal', 'full-width', 'full-height', 'bg-color-transparent-shade', 'pd-md');

var BUTTON_CLASSES = (0, _classnames2.default)('position-absolute', 'align-items-center', 'display-flex', 'pd-right-lg', 'right-lg', 'top-lg', 'color-white');

var IMAGE_CLASSES = (0, _classnames2.default)('position-absolute', 'bg-color-white', 'transform-translate-minus-50-minus-50');

var IMAGE_STYLES = {
  boxShadow: '0 0 1px #222',
  left: '50%',
  top: '50%',
  maxHeight: '100%',
  maxWidth: '100%'
};

var LightBox = function (_Component) {
  _inherits(LightBox, _Component);

  function LightBox() {
    _classCallCheck(this, LightBox);

    return _possibleConstructorReturn(this, (LightBox.__proto__ || Object.getPrototypeOf(LightBox)).apply(this, arguments));
  }

  _createClass(LightBox, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          onClose = _props.onClose,
          open = _props.open,
          src = _props.src,
          topOffset = _props.topOffset;


      if (open) {
        return _react2.default.createElement(
          'div',
          { key: 'showLightbox', className: BACKGROUND_CLASSES,
            style: { backgroundColor: 'rgba(0, 0, 0, 0.7)', top: topOffset }, onClick: onClose },
          _react2.default.createElement(_Button2.default, {
            borderless: true,
            className: BUTTON_CLASSES,
            icon: 'times',
            text: 'Close',
            onClick: onClose,
            size: 'lg'
          }),
          _react2.default.createElement('img', { className: IMAGE_CLASSES, src: src, style: IMAGE_STYLES, onClick: function onClick(e) {
              return e.stopPropagation();
            } })
        );
      } else {
        return null;
      }
    }
  }]);

  return LightBox;
}(_react.Component);

LightBox.propTypes = {
  open: _propTypes2.default.bool.isRequired,
  onClose: _propTypes2.default.func.isRequired,
  src: _propTypes2.default.string.isRequired,
  topOffset: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
};
LightBox.defaultProps = {
  topOffset: 0
};
exports.default = LightBox;