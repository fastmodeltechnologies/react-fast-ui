'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Icon = require('../Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderCell = function (_Component) {
  _inherits(HeaderCell, _Component);

  function HeaderCell() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, HeaderCell);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = HeaderCell.__proto__ || Object.getPrototypeOf(HeaderCell)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (e) {
      var onSort = _this.props.onSort;

      if (onSort !== null) {
        onSort();
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(HeaderCell, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          style = _props.style,
          children = _props.children,
          sorted = _props.sorted,
          onSort = _props.onSort,
          rest = _objectWithoutProperties(_props, ['style', 'children', 'sorted', 'onSort']);

      return _react2.default.createElement(
        'th',
        _extends({ style: _extends({
            cursor: onSort !== null ? 'pointer' : null,
            borderBottom: sorted ? '3px solid #e65d1d' : null,
            padding: 8,
            verticalAlign: 'bottom'
          }, style) }, rest, { onClick: this.handleClick }),
        _react2.default.createElement(
          'div',
          { className: 'display-flex align-items-center' },
          _react2.default.createElement(
            'div',
            { className: 'flex-grow-1' },
            children
          ),
          _react2.default.createElement(
            'div',
            { className: 'flex-shrink-0' },
            sorted === null ? null : _react2.default.createElement(_Icon2.default, { name: sorted === 'D' ? 'angle-down' : 'angle-up', className: 'mg-left-sm' })
          )
        )
      );
    }
  }], [{
    key: 'getNewSortInfo',


    /**
     * Helper function for getting the new sort info based on the old sort info and the sorted attribute
     * @param oldSortInfo old sort info object
     * @param attribute attribute sorted on
     * @returns {{attribute: *, desc: boolean}}
     */
    value: function getNewSortInfo(oldSortInfo, attribute) {
      return {
        attribute: attribute,
        desc: oldSortInfo && oldSortInfo.attribute === attribute ? !oldSortInfo.desc : true
      };
    }

    /**
     * Helper function that returns the sorted and onSort attributes based on the sortInfo and the attribute name
     * @param sortInfo
     * @param onSortAttribute
     * @param attribute
     * @returns {{sorted: *, onSort: *}}
     */

  }, {
    key: 'sortAttributes',
    value: function sortAttributes() {
      var sortInfo = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var onSortAttribute = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var attribute = arguments[2];

      var sorted = null;
      if (sortInfo !== null) {
        if (sortInfo.attribute === attribute) {
          sorted = sortInfo.desc ? 'D' : 'A';
        }
      }

      var onSort = null;

      if (onSortAttribute !== null) {
        onSort = function onSort() {
          return onSortAttribute(HeaderCell.getNewSortInfo(sortInfo, attribute));
        };
      }

      return { sorted: sorted, onSort: onSort };
    }
  }]);

  return HeaderCell;
}(_react.Component);

HeaderCell.propTypes = {
  onSort: _propTypes2.default.func,
  sorted: _propTypes2.default.oneOf(['A', 'D'])
};
HeaderCell.defaultProps = {
  sorted: null,
  onSort: null
};
exports.default = HeaderCell;