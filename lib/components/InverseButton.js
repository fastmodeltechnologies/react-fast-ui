'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SimpleButton = require('./SimpleButton');

var _SimpleButton2 = _interopRequireDefault(_SimpleButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var getBgColorClasses = function getBgColorClasses(type) {
  return 'bg-color-white color-' + type;
};
var getBorderClasses = function getBorderClasses(type) {
  return 'border-width-1px border-style-solid border-radius-xxl border-color-' + type + ' hover-border-color-' + type + '-darken';
};
var getDisabledBorderClasses = function getDisabledBorderClasses() {
  return 'border-width-1px border-style-solid border-radius-xxl border-color-grey-darken';
};

var InverseButton = function (_Component) {
  _inherits(InverseButton, _Component);

  function InverseButton() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, InverseButton);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = InverseButton.__proto__ || Object.getPrototypeOf(InverseButton)).call.apply(_ref, [this].concat(args))), _this), _this.beforeOnClick = function (e) {
      return _this.props.onClick ? _this.props.onClick(e) : null;
    }, _this.focus = function () {
      return _this.refs.button.focus();
    }, _this.getPaddingClasses = function () {
      var size = _this.props.size;

      var verticalPadding = size === 'sm' ? 'xs' : 'sm';

      return 'pd-top-' + verticalPadding + ' pd-right-' + size + ' pd-bottom-' + verticalPadding + ' pd-left-' + size;
    }, _this.getHoveringClasses = function () {
      var _this$props = _this.props,
          type = _this$props.type,
          disabled = _this$props.disabled;

      if (disabled) {
        return;
      }
      return type !== 'primary' ? 'color-' + type + ' hover-color-' + type + '-darken' : 'hover-bg-color-primary hover-color-white';
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(InverseButton, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          className = _props.className,
          disabled = _props.disabled,
          href = _props.href,
          size = _props.size,
          style = _props.style,
          tabIndex = _props.tabIndex,
          type = _props.type,
          rest = _objectWithoutProperties(_props, ['children', 'className', 'disabled', 'href', 'size', 'style', 'tabIndex', 'type']);

      var classNames = (0, _classnames2.default)(this.getHoveringClasses(), 'outline-none', disabled ? 'color-light-gray' : getBgColorClasses(type), disabled ? getDisabledBorderClasses() : getBorderClasses(type), disabled ? 'cursor-not-allowed' : 'cursor-pointer', size + '-font', this.getPaddingClasses(), className);
      var styles = _extends({ textDecoration: 'none' }, style);
      var onClick = function onClick(e) {
        return disabled ? null : _this2.beforeOnClick(e);
      };

      return _react2.default.createElement(
        'a',
        _extends({}, rest, {
          className: classNames,
          style: styles,
          ref: 'button',
          href: href,
          onClick: onClick,
          tabIndex: tabIndex }),
        children
      );
    }
  }]);

  return InverseButton;
}(_react.Component);

InverseButton.sizes = _SimpleButton2.default.sizes;
InverseButton.types = _SimpleButton2.default.types;
InverseButton.propTypes = {
  disabled: _propTypes2.default.bool,
  href: _propTypes2.default.string,
  onClick: _propTypes2.default.func,
  size: _propTypes2.default.oneOf(InverseButton.sizes),
  tabIndex: _propTypes2.default.string,
  type: _propTypes2.default.oneOf(InverseButton.types)
};
InverseButton.defaultProps = {
  disabled: false,
  href: null,
  onClick: null,
  size: 'md',
  tabIndex: null,
  type: 'primary'
};
exports.default = InverseButton;