'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UP_ARROW = 'angle-up';
var DOWN_ARROW = 'angle-down';

var Drawer = function (_Component) {
  _inherits(Drawer, _Component);

  function Drawer() {
    _classCallCheck(this, Drawer);

    return _possibleConstructorReturn(this, (Drawer.__proto__ || Object.getPrototypeOf(Drawer)).apply(this, arguments));
  }

  _createClass(Drawer, [{
    key: 'getDrawerClasses',
    value: function getDrawerClasses() {
      var open = this.props.open;

      return (0, _classnames2.default)({ 'display-none': !open }, 'list-unstyled');
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          buttonClasses = _props.buttonClasses,
          children = _props.children,
          className = _props.className,
          downArrowIcon = _props.downArrowIcon,
          icon = _props.icon,
          onClose = _props.onClose,
          onOpen = _props.onOpen,
          open = _props.open,
          upArrowIcon = _props.upArrowIcon;

      // props for the button that opens and closes the drawer

      var buttonProps = Object.assign({}, this.props, {
        borderless: true,
        className: buttonClasses || '',
        icon: icon || (open ? upArrowIcon : downArrowIcon)
      });

      return _react2.default.createElement(
        'div',
        { className: className },
        _react2.default.createElement(_Button2.default, _extends({}, buttonProps, { onClick: open ? onClose : onOpen })),
        _react2.default.createElement(
          'ul',
          { className: this.getDrawerClasses() },
          _react2.default.Children.map(children, function (child) {
            return _react2.default.createElement(
              'li',
              { className: 'mg-left-sm' },
              _react2.default.cloneElement(child)
            );
          })
        )
      );
    }
  }]);

  return Drawer;
}(_react.Component);

Drawer.propTypes = {
  downArrowIcon: _propTypes2.default.string,
  icon: _propTypes2.default.string,
  menuClasses: _propTypes2.default.string,
  upArrowIcon: _propTypes2.default.string
};
Drawer.defaultProps = {
  downArrowIcon: DOWN_ARROW,
  icon: null,
  upArrowIcon: UP_ARROW
};
exports.default = Drawer;