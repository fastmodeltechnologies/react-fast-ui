'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BUTTON_SIZES = ['sm', 'md', 'lg'];
var BUTTON_TYPES = ['primary', 'secondary', 'default', 'danger', 'success', 'warning'];
var ICON_CLASSES = (0, _classnames2.default)('display-inline-block', 'vertical-align-middle');

var Button = function (_Component) {
  _inherits(Button, _Component);

  function Button() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Button);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Button.__proto__ || Object.getPrototypeOf(Button)).call.apply(_ref, [this].concat(args))), _this), _this.state = { lastClick: 0 }, _this.focus = function () {
      return _this.refs.button.focus();
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Button, [{
    key: 'beforeOnClick',
    value: function beforeOnClick(e) {
      var _props = this.props,
          clickDelay = _props.clickDelay,
          submit = _props.submit,
          href = _props.href,
          onClick = _props.onClick;

      var now = new Date().getTime();

      // if this isn't a submit button, then
      // prevent the default action of submitting the form
      if (!submit && !href) {
        e.preventDefault();
      }

      if (now - this.state.lastClick <= clickDelay) {
        return false;
      }

      this.setState({ lastClick: now });

      if (onClick) {
        onClick(e);
      }
    }
  }, {
    key: 'getBorderClasses',
    value: function getBorderClasses() {
      var borderless = this.props.borderless;

      if (borderless) {
        return 'border-none border-radius-xxl';
      }

      return 'border-width-1px border-style-solid border-radius-xxl';
    }
  }, {
    key: 'getColorClasses',
    value: function getColorClasses() {
      var _props2 = this.props,
          disabled = _props2.disabled,
          borderless = _props2.borderless;
      var _props3 = this.props,
          type = _props3.type,
          inverse = _props3.inverse;

      // handle the disabled state first since they look the same

      if (disabled) {
        if (borderless) {
          return 'bg-color-transparent color-lightest-gray';
        } else {
          return 'bg-color-light-gray color-white';
        }
      }

      if (type == 'default') {
        type = 'primary';
        inverse = !inverse;
      }

      // the common case
      if (borderless) {
        return 'bg-color-transparent color-' + type + ' hover-color-' + type + '-darken';
      } else {
        if (inverse) {
          return 'bg-color-white color-' + type + ' border-color-' + type + ' hover-bg-color-' + type + ' hover-color-white';
        } else {
          return 'bg-color-' + type + ' hover-bg-color-' + type + '-darken border-color-' + type + '-darken color-white';
        }
      }
    }
  }, {
    key: 'getPaddingClasses',
    value: function getPaddingClasses() {
      var size = this.props.size;


      var verticalPadding = size === 'sm' ? 'xs' : 'sm';

      return 'pd-top-' + verticalPadding + ' pd-right-' + size + ' pd-bottom-' + verticalPadding + ' pd-left-' + size;
    }
  }, {
    key: 'getButtonClasses',
    value: function getButtonClasses() {
      var _props4 = this.props,
          disabled = _props4.disabled,
          className = _props4.className;


      return (0, _classnames2.default)(this.getBorderClasses(), this.getColorClasses(), this.getPaddingClasses(), {
        'cursor-pointer': !disabled
      }, 'outline-none', 'hover-visibility-parent', className);
    }
  }, {
    key: 'getTextClasses',
    value: function getTextClasses() {
      var _props5 = this.props,
          borderless = _props5.borderless,
          hoverText = _props5.hoverText,
          icon = _props5.icon,
          size = _props5.size,
          text = _props5.text;


      var marginClass = icon ? 'mg-left-sm' : '';

      return (0, _classnames2.default)(borderless && !text ? 'display-none' : 'display-inline-block', size + '-font', {
        'visibility-hidden': hoverText,
        'visibility-child-visible': hoverText
      }, marginClass);
    }
  }, {
    key: 'commonProps',
    value: function commonProps() {
      var _props6 = this.props,
          disabled = _props6.disabled,
          loading = _props6.loading,
          tabIndex = _props6.tabIndex;


      return {
        className: this.getButtonClasses(),
        disabled: disabled || loading,
        tabIndex: tabIndex
      };
    }
  }, {
    key: 'renderIcon',
    value: function renderIcon() {
      var _props7 = this.props,
          borderless = _props7.borderless,
          icon = _props7.icon,
          loadingIcon = _props7.loadingIcon,
          loading = _props7.loading,
          size = _props7.size,
          buttonIconStyle = _props7.buttonIconStyle;


      var iconSize = {
        sm: borderless ? '1x' : '1x',
        md: borderless ? 'lg' : 'lg',
        lg: borderless ? '2x' : 'lg'
      };

      var iconToShow = loading && loadingIcon !== null ? loadingIcon : icon;

      if (iconToShow) {
        if (typeof iconToShow === 'string') {
          return _react2.default.createElement(_Icon2.default, { key: 'icon', className: ICON_CLASSES, name: iconToShow,
            size: iconSize[size], style: buttonIconStyle });
        } else {
          return iconToShow;
        }
      } else {
        return null;
      }
    }
  }, {
    key: 'renderText',
    value: function renderText() {
      var _props8 = this.props,
          text = _props8.text,
          loadingText = _props8.loadingText,
          loading = _props8.loading,
          buttonTextStyle = _props8.buttonTextStyle;

      var spanContent = loading && loadingText !== null ? loadingText : text;

      return _react2.default.createElement(
        'span',
        { key: 'text', className: this.getTextClasses(), style: buttonTextStyle },
        spanContent
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props9 = this.props,
          disabled = _props9.disabled,
          href = _props9.href,
          submit = _props9.submit,
          style = _props9.style;


      if (href && !disabled) {
        // Render an <a> if we've passed in an href, and the button is not disabled
        return _react2.default.createElement(
          'a',
          _extends({
            ref: 'button',
            href: href,
            style: style,
            onClick: function onClick(e) {
              return _this2.beforeOnClick(e);
            }
          }, this.commonProps()),
          this.renderIcon(),
          this.renderText()
        );
      } else {
        // Render a <button> if we've not passed in an href
        return _react2.default.createElement(
          'button',
          _extends({
            ref: 'button',
            style: style,
            onClick: function onClick(e) {
              return _this2.beforeOnClick(e);
            },
            type: submit ? 'submit' : 'button'
          }, this.commonProps()),
          this.renderIcon(),
          this.renderText()
        );
      }
    }
  }]);

  return Button;
}(_react.Component);

Button.types = BUTTON_TYPES;
Button.sizes = BUTTON_SIZES;
Button.propTypes = {
  block: _propTypes2.default.bool,
  borderless: _propTypes2.default.bool,
  className: _propTypes2.default.string,
  clickDelay: _propTypes2.default.number,
  disabled: _propTypes2.default.bool,
  href: _propTypes2.default.string,
  hoverText: _propTypes2.default.bool,
  inverse: _propTypes2.default.bool,
  icon: _propTypes2.default.node,
  loading: _propTypes2.default.bool,
  loadingIcon: _propTypes2.default.node,
  loadingText: _propTypes2.default.node,
  onClick: _propTypes2.default.func,
  size: _propTypes2.default.oneOf(BUTTON_SIZES),
  submit: _propTypes2.default.bool,
  tabIndex: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  type: _propTypes2.default.oneOf(BUTTON_TYPES),
  text: _propTypes2.default.node,
  style: _propTypes2.default.object,
  buttonIconStyle: _propTypes2.default.object,
  buttonTextStyle: _propTypes2.default.object
};
Button.defaultProps = {
  block: false,
  borderless: false,
  clickDelay: 200,
  disabled: false,
  href: null,
  hoverText: false,
  inverse: false,
  icon: null,
  loading: false,
  loadingIcon: null,
  loadingText: null,
  onClick: null,
  size: 'md',
  submit: false,
  type: 'default',
  text: null,
  style: null,
  buttonIconStyle: null,
  buttonTextStyle: null
};
exports.default = Button;