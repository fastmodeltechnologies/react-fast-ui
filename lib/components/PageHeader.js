'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PageSubHeader = function (_PureComponent) {
  _inherits(PageSubHeader, _PureComponent);

  function PageSubHeader() {
    _classCallCheck(this, PageSubHeader);

    return _possibleConstructorReturn(this, (PageSubHeader.__proto__ || Object.getPrototypeOf(PageSubHeader)).apply(this, arguments));
  }

  _createClass(PageSubHeader, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          className = _props.className,
          style = _props.style;

      var props = {
        className: (0, _classnames2.default)('bg-color-blue-gray', 'full-width', 'pd-bottom-sm', 'pd-top-sm', className),
        style: _extends({
          minHeight: 42
        }, style)
      };

      return _react2.default.createElement(
        'div',
        props,
        children
      );
    }
  }]);

  return PageSubHeader;
}(_react.PureComponent);

var ICON_STYLES = { bottom: -24, marginLeft: 20, zIndex: -1 };

var HEADER_CLASSES = (0, _classnames2.default)('display-flex', 'align-items-flex-end', 'justify-content-space-between', 'max-width', 'sm-down-flex-direction-column', 'sm-down-align-items-center', 'mg-bottom-sm');

var H1_CLASSES = (0, _classnames2.default)('display-flex', 'flex-grow-1', 'full-height', 'align-items-flex-end', 'pd-bottom-sm', 'mg-none', 'position-relative');

var PageHeader = function (_Component) {
  _inherits(PageHeader, _Component);

  function PageHeader() {
    _classCallCheck(this, PageHeader);

    return _possibleConstructorReturn(this, (PageHeader.__proto__ || Object.getPrototypeOf(PageHeader)).apply(this, arguments));
  }

  _createClass(PageHeader, [{
    key: 'getIconStyle',
    value: function getIconStyle() {
      var _props2 = this.props,
          bottomOffsetPixels = _props2.bottomOffsetPixels,
          iconSize = _props2.iconSize;


      return _extends({}, ICON_STYLES, {
        bottom: bottomOffsetPixels * -1 + 'px',
        fontSize: iconSize
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          children = _props3.children,
          className = _props3.className,
          icon = _props3.icon,
          title = _props3.title;


      return _react2.default.createElement(
        'header',
        { className: (0, _classnames2.default)('hidden-print', className) },
        _react2.default.createElement(
          'div',
          { className: 'mg-bottom-sm' },
          _react2.default.createElement(
            'section',
            { key: 'head', className: HEADER_CLASSES, style: { minHeight: 120 } },
            _react2.default.createElement(
              'h1',
              { key: 'left', className: H1_CLASSES },
              title,
              _react2.default.createElement(_Icon2.default, {
                name: icon,
                style: this.getIconStyle(),
                className: 'position-absolute color-lightest-gray hidden-xs'
              })
            ),
            _react2.default.createElement(
              'aside',
              { key: 'right', className: 'pd-bottom-sm' },
              children
            )
          )
        )
      );
    }
  }]);

  return PageHeader;
}(_react.Component);

PageHeader.Subhead = PageSubHeader;
PageHeader.propTypes = {
  title: _propTypes2.default.string.isRequired,
  icon: _propTypes2.default.string,
  bottomOffsetPixels: _propTypes2.default.number,
  iconSize: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
};
PageHeader.defaultProps = {
  bottomOffsetPixels: 20,
  icon: 'dribbble fa-rotate-56',
  iconSize: 100
};
exports.default = PageHeader;