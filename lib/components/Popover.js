'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

var _scrollparent = require('scrollparent');

var _scrollparent2 = _interopRequireDefault(_scrollparent);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _Portal = require('./Portal');

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// this function returns a number within some bounds
var bound = function bound(num, min, max) {
  return Math.max(Math.min(num, max), min);
};

var BASE_ARROW_STYLES = {
  width: 0,
  height: 0,
  borderStyle: 'solid',
  borderTopColor: 'transparent',
  borderBottomColor: 'transparent',
  borderLeftColor: 'transparent',
  borderRightColor: 'transparent'
};

var EMPTY_PLACEMENT_INFO = {
  popoverStyle: null,
  arrowStyle: null
};

var PLACEMENT_TOP = 'top';
var PLACEMENT_RIGHT = 'right';
var PLACEMENT_BOTTOM = 'bottom';
var PLACEMENT_LEFT = 'left';
var PLACEMENTS = [PLACEMENT_TOP, PLACEMENT_RIGHT, PLACEMENT_BOTTOM, PLACEMENT_LEFT];

/**
 * This element is used to show content surrounding some element, such as a tooltip or additional action buttons
 */

var Popover = function (_PureComponent) {
  _inherits(Popover, _PureComponent);

  function Popover() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Popover);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Popover.__proto__ || Object.getPrototypeOf(Popover)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      positionData: null
    }, _this.setClientRects = function () {
      var content = _this.refs.content,
          children = _this.getChildNode(),
          positionData = _this.state.positionData;


      if (children && content && window) {
        var _children$getBounding = children.getBoundingClientRect(),
            childTop = _children$getBounding.top,
            childRight = _children$getBounding.right,
            childBottom = _children$getBounding.bottom,
            childLeft = _children$getBounding.left,
            _content$getBoundingC = content.getBoundingClientRect(),
            contentWidth = _content$getBoundingC.width,
            contentHeight = _content$getBoundingC.height,
            _window = window,
            windowHeight = _window.innerHeight,
            windowWidth = _window.innerWidth;

        var newPositionData = {
          childTop: childTop,
          childRight: childRight,
          childBottom: childBottom,
          childLeft: childLeft,
          contentWidth: contentWidth,
          contentHeight: contentHeight,
          windowHeight: windowHeight,
          windowWidth: windowWidth
        };

        if (!_underscore2.default.isEqual(positionData, newPositionData)) {
          _this.setState({ positionData: newPositionData }, _this.setClientRects);
        }
      }
    }, _this._throttledSetClientRect = _underscore2.default.throttle(function () {
      var open = _this.props.open;

      if (open) {
        _this.setClientRects();
      }
    }, 30), _this.setChildRef = function (child) {
      return _this._children = child;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  // in the state we store information used in the positioning of the popover


  _createClass(Popover, [{
    key: 'getChildNode',


    // this element will try to wrap a single child if that's all it gets
    value: function getChildNode() {
      var _children = this._children;

      if (_children) {
        return (0, _reactDom.findDOMNode)(_children);
      }

      return null;
    }

    /**
     * Copy information about the bounding rectangle of the child and the popover content dimensions into the state
     */

  }, {
    key: 'componentDidMount',


    // Bind event listeners to calculate a new position of the popover
    value: function componentDidMount() {
      window.addEventListener('scroll', this._throttledSetClientRect);
      window.addEventListener('resize', this._throttledSetClientRect);

      this._scrollParent = (0, _scrollparent2.default)(this.getChildNode());
      this._scrollParent.addEventListener('scroll', this._throttledSetClientRect);

      this._throttledSetClientRect();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this._throttledSetClientRect);
      window.removeEventListener('resize', this._throttledSetClientRect);

      this._scrollParent.removeEventListener('scroll', this._throttledSetClientRect);
    }

    /**
     * Check to see that the scroll parent listener is correct
     */

  }, {
    key: 'updateScrollParentListener',
    value: function updateScrollParentListener() {
      var scrollParent = (0, _scrollparent2.default)(this.getChildNode());

      if (this._scrollParent !== scrollParent) {
        this._scrollParent.removeEventListener('scroll', this._throttledSetClientRect);
        scrollParent.addEventListener('scroll', this._throttledSetClientRect);
        this._scrollParent = scrollParent;
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var _this2 = this;

      this.updateScrollParentListener();

      var open = this.props.open;
      var prevOpen = prevProps.open;

      // If popover has just opened, update client rects after a short delay to ensure
      // the content ref element has an accurate width.

      if (open && !prevOpen) {
        window.setTimeout(function () {
          return _this2.setClientRects();
        }, 100);
      }
    }

    /**
     * Calculate the placement style of the fixed popover container
     * @returns {*}
     */

  }, {
    key: 'getPlacementStyle',
    value: function getPlacementStyle() {
      var _fitsIn;

      var positionData = this.state.positionData,
          _props = this.props,
          buffer = _props.buffer,
          arrowSize = _props.arrowSize,
          arrowColor = _props.arrowColor,
          defaultPlacement = _props.defaultPlacement;


      if (positionData === null) {
        return EMPTY_PLACEMENT_INFO;
      }

      var contentHeight = positionData.contentHeight,
          contentWidth = positionData.contentWidth,
          windowHeight = positionData.windowHeight,
          windowWidth = positionData.windowWidth,
          childTop = positionData.childTop,
          childRight = positionData.childRight,
          childBottom = positionData.childBottom,
          childLeft = positionData.childLeft;

      // the dimensions of the child

      var childWidth = childRight - childLeft,
          childHeight = childBottom - childTop;

      // calculate whether it fits in each of the four different placements
      var fitsIn = (_fitsIn = {}, _defineProperty(_fitsIn, PLACEMENT_TOP, childTop >= contentHeight + childHeight + buffer + arrowSize && childLeft >= buffer && childRight <= windowWidth - buffer), _defineProperty(_fitsIn, PLACEMENT_RIGHT, childRight + contentWidth + buffer + arrowSize <= windowWidth && childTop >= buffer && childBottom <= windowHeight - buffer), _defineProperty(_fitsIn, PLACEMENT_BOTTOM, childBottom + contentHeight + buffer + arrowSize <= windowHeight && childLeft >= buffer && childRight <= windowWidth - buffer), _defineProperty(_fitsIn, PLACEMENT_LEFT, childLeft - contentWidth - buffer - arrowSize >= 0 && childTop >= buffer && childBottom <= windowHeight - buffer), _fitsIn);

      var placement = defaultPlacement;

      if (!fitsIn[defaultPlacement]) {
        placement = _underscore2.default.chain(fitsIn).omit(function (val) {
          return !val;
        }).keys().first().value() || defaultPlacement;
      }

      switch (placement) {
        case PLACEMENT_TOP:
          return {
            popoverPlacement: {
              top: childTop - contentHeight - arrowSize,
              left: bound(childLeft + childWidth / 2 - contentWidth / 2, buffer, windowWidth - buffer - contentWidth)
            },

            arrowPlacement: {
              top: childTop - arrowSize,
              left: childLeft + childWidth / 2 - arrowSize,
              borderWidth: arrowSize,
              borderTopColor: arrowColor
            }
          };
        case PLACEMENT_RIGHT:
          return {
            popoverPlacement: {
              top: bound(childTop + childHeight / 2 - contentHeight / 2, buffer, windowHeight - buffer - contentHeight),
              left: childRight + arrowSize
            },

            arrowPlacement: {
              left: childRight - arrowSize,
              top: childTop + childHeight / 2 - arrowSize,
              borderWidth: arrowSize,
              borderRightColor: arrowColor
            }
          };
        case PLACEMENT_BOTTOM:
          return {
            popoverPlacement: {
              top: childBottom + arrowSize,
              left: bound(childLeft + childWidth / 2 - contentWidth / 2, buffer, windowWidth - buffer - contentWidth)
            },

            arrowPlacement: {
              left: childLeft + childWidth / 2 - arrowSize,
              top: childTop + childHeight - arrowSize,
              borderWidth: arrowSize,
              borderBottomColor: arrowColor
            }
          };
        case PLACEMENT_LEFT:
          return {
            popoverPlacement: {
              top: bound(childTop + childHeight / 2 - contentHeight / 2, buffer, windowHeight - buffer - contentHeight),
              left: childLeft - contentWidth - arrowSize
            },

            arrowPlacement: {
              top: childTop + childHeight / 2 - arrowSize,
              left: childLeft - arrowSize,
              borderWidth: arrowSize,
              borderLeftColor: arrowColor
            }
          };
        default:
          return EMPTY_PLACEMENT_INFO;
      }
    }
  }, {
    key: 'getChildren',
    value: function getChildren() {
      var children = this.props.children;


      if (_react.Children.count(children) === 1) {
        return (0, _react.cloneElement)(children, { ref: this.setChildRef });
      }

      return _react2.default.createElement(
        'div',
        { ref: this.setChildRef, style: { display: 'inline-block' } },
        children
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          content = _props2.content,
          open = _props2.open,
          arrowClassName = _props2.arrowClassName,
          popoverClassName = _props2.popoverClassName,
          style = _props2.style,
          arrowStyle = _props2.arrowStyle,
          popoverStyle = _props2.popoverStyle;

      var _getPlacementStyle = this.getPlacementStyle(),
          popoverPlacement = _getPlacementStyle.popoverPlacement,
          arrowPlacement = _getPlacementStyle.arrowPlacement;

      return _react2.default.createElement(
        'span',
        null,
        this.getChildren(),
        _react2.default.createElement(
          _Portal2.default,
          null,
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'div',
              { ref: 'content', className: popoverClassName,
                style: _extends({
                  position: 'fixed',
                  display: 'inline-block',
                  visibility: !open ? 'hidden' : null
                }, style, popoverPlacement, popoverStyle) },
              content
            ),
            _react2.default.createElement('div', { ref: 'arrow',
              className: arrowClassName,
              style: _extends({
                position: 'fixed',
                visibility: !open ? 'hidden' : null
              }, style, BASE_ARROW_STYLES, arrowPlacement, arrowStyle) })
          )
        )
      );
    }
  }]);

  return Popover;
}(_react.PureComponent);

Popover.PLACEMENTS = PLACEMENTS;
Popover.propTypes = {
  open: _propTypes2.default.bool.isRequired,
  content: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node.isRequired]).isRequired,
  buffer: _propTypes2.default.number,
  arrowSize: _propTypes2.default.number,

  arrowClassName: _propTypes2.default.string,
  arrowStyle: _propTypes2.default.object,
  arrowColor: _propTypes2.default.string,
  popoverClassName: _propTypes2.default.string,
  popoverStyle: _propTypes2.default.object,
  defaultPlacement: _propTypes2.default.oneOf([PLACEMENT_TOP, PLACEMENT_RIGHT, PLACEMENT_BOTTOM, PLACEMENT_LEFT])
};
Popover.defaultProps = {
  buffer: 8,
  arrowSize: 6,
  arrowColor: 'white',

  popoverStyle: { borderRadius: 4, boxShadow: '0 2px 5px 2px #c4c4c4', backgroundColor: 'white' },
  popoverClassName: null,
  arrowStyle: null,
  arrowClassName: null,
  defaultPlacement: PLACEMENT_TOP,
  style: { zIndex: 100 }
};
exports.default = Popover;