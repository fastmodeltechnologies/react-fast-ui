'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BLANK_PLACEHOLDER = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

var TeamLogo = function (_Component) {
  _inherits(TeamLogo, _Component);

  function TeamLogo() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, TeamLogo);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = TeamLogo.__proto__ || Object.getPrototypeOf(TeamLogo)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      errorPlaceHolder: false
    }, _this.getDimensions = function () {
      var _this$props = _this.props,
          height = _this$props.height,
          width = _this$props.width,
          style = _this$props.style,
          fullSizePlaceholder = _this$props.fullSizePlaceholder;

      var maxWidth = void 0;
      var placeholder = void 0;

      if (width === 'auto') {
        maxWidth = 2 * height;
      }

      placeholder = fullSizePlaceholder && _this.state.errorPlaceHolder ? { marginRight: 10 } : {};

      if (_this.state.errorPlaceHolder && !fullSizePlaceholder) {
        return { width: 0, height: 0 };
      }

      return _extends({ fontSize: height, height: height, width: width, maxWidth: maxWidth, objectFit: 'scale-down', padding: 2 }, style, placeholder);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(TeamLogo, [{
    key: 'render',


    // lifecycle
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('img', {
        src: this.state.errorPlaceHolder || this.props.url,
        onError: function onError(e) {
          e.target.onerror = null;
          _this2.setState({ errorPlaceHolder: BLANK_PLACEHOLDER });
        },
        style: this.getDimensions()
      });
    }
  }]);

  return TeamLogo;
}(_react.Component);

TeamLogo.propTypes = {
  url: _propTypes2.default.string.isRequired,
  height: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  width: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string])
};
TeamLogo.defaultProps = {
  height: 100,
  width: 'auto'
};
exports.default = TeamLogo;