'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactTransitionGroup = require('react-transition-group');

var _Portal = require('./Portal');

var _Portal2 = _interopRequireDefault(_Portal);

var _reactFixedPageMs = require('react-fixed-page-ms');

var _reactFixedPageMs2 = _interopRequireDefault(_reactFixedPageMs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SidebarTransition = function SidebarTransition(props) {
  return _react2.default.createElement(_reactTransitionGroup.TransitionGroup, props);
};

var Sidebar = function (_Component) {
  _inherits(Sidebar, _Component);

  function Sidebar() {
    _classCallCheck(this, Sidebar);

    return _possibleConstructorReturn(this, (Sidebar.__proto__ || Object.getPrototypeOf(Sidebar)).apply(this, arguments));
  }

  _createClass(Sidebar, [{
    key: 'renderSidebar',
    value: function renderSidebar() {
      var _props = this.props,
          open = _props.open,
          children = _props.children,
          onClose = _props.onClose,
          backdrop = _props.backdrop,
          rest = _objectWithoutProperties(_props, ['open', 'children', 'onClose', 'backdrop']);

      if (!open) {
        return null;
      }

      return _react2.default.createElement(
        _reactTransitionGroup.CSSTransition,
        { key: 'sidebar', classNames: 'sidebar', timeout: { enter: 400, exit: 500 } },
        _react2.default.createElement(
          'div',
          { className: 'sidebar' },
          backdrop ? _react2.default.createElement('div', { className: 'sidebar-backdrop', onClick: onClose }) : null,
          _react2.default.createElement(
            'div',
            _extends({ className: 'sidebar-content' }, rest),
            children
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(
          _Portal2.default,
          null,
          _react2.default.createElement(
            SidebarTransition,
            null,
            this.renderSidebar()
          )
        ),
        _react2.default.createElement(_reactFixedPageMs2.default, { fixed: this.props.open })
      );
    }
  }]);

  return Sidebar;
}(_react.Component);

Sidebar.propTypes = {
  open: _propTypes2.default.bool.isRequired,
  onClose: _propTypes2.default.func.isRequired,
  backdrop: _propTypes2.default.bool
};
Sidebar.defaultProps = {
  backdrop: true
};
exports.default = Sidebar;