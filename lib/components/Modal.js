'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactTransitionGroup = require('react-transition-group');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Portal = require('./Portal');

var _Portal2 = _interopRequireDefault(_Portal);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _reactFixedPageMs = require('react-fixed-page-ms');

var _reactFixedPageMs2 = _interopRequireDefault(_reactFixedPageMs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var ESCAPE_KEYCODE = 27;

var ModalBody = function ModalBody(_ref) {
  var className = _ref.className,
      rest = _objectWithoutProperties(_ref, ['className']);

  return _react2.default.createElement('section', _extends({ className: (0, _classnames2.default)('pd-md', className) }, rest));
};

var ModalFooter = function ModalFooter(_ref2) {
  var className = _ref2.className,
      children = _ref2.children,
      rest = _objectWithoutProperties(_ref2, ['className', 'children']);

  return _react2.default.createElement(
    'footer',
    _extends({ className: (0, _classnames2.default)('display-flex justify-content-flex-end pd-md', className) }, rest),
    _react2.default.createElement(
      'div',
      { className: 'flex-shrink-0' },
      children
    )
  );
};

var CloseButton = function CloseButton(_ref3) {
  var closeButton = _ref3.closeButton,
      rest = _objectWithoutProperties(_ref3, ['closeButton']);

  return _react2.default.createElement(
    'div',
    _extends({ className: 'flex-shrink-0 cursor-pointer color-light-gray hover-color-fastmodel-gray' }, rest),
    closeButton || _react2.default.createElement(_Icon2.default, { name: 'times' })
  );
};

var ModalTitle = function ModalTitle(_ref4) {
  var className = _ref4.className,
      onClose = _ref4.onClose,
      children = _ref4.children,
      style = _ref4.style,
      rest = _objectWithoutProperties(_ref4, ['className', 'onClose', 'children', 'style']);

  return _react2.default.createElement(
    'header',
    { className: (0, _classnames2.default)('pd-md display-flex flex-direction-row align-items-center', className),
      style: _extends({ borderBottom: '1px solid rgba(0,0,0,0.2)' }, style) },
    _react2.default.createElement(
      'h4',
      { style: { margin: 0 }, className: 'flex-grow-1' },
      children
    ),
    onClose != null ? _react2.default.createElement(CloseButton, { onClick: function onClick() {
        return onClose();
      } }) : null
  );
};

var ModalTransition = function ModalTransition(props) {
  return _react2.default.createElement(_reactTransitionGroup.TransitionGroup, props);
};

var Modal = function (_PureComponent) {
  _inherits(Modal, _PureComponent);

  function Modal() {
    var _ref5;

    var _temp, _this, _ret;

    _classCallCheck(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref5 = Modal.__proto__ || Object.getPrototypeOf(Modal)).call.apply(_ref5, [this].concat(args))), _this), _this.focusModalSection = function () {
      var section = _this.refs.section;

      if (section) {
        section.focus();
      }
    }, _this.handleBackdropClick = function () {
      var _this$props = _this.props,
          backdrop = _this$props.backdrop,
          onClose = _this$props.onClose;


      if (backdrop !== 'static' && backdrop !== 'full') {
        onClose();
      }
    }, _this.handleKeyDown = function (_ref6) {
      var keyCode = _ref6.keyCode;
      var onClose = _this.props.onClose;


      if (keyCode === ESCAPE_KEYCODE) {
        onClose();
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Modal, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var open = this.props.open;


      if (open && !prevProps.open) {
        // we need a timeout because this should happen *after* the portal renders it to the DOM
        setTimeout(this.focusModalSection, 0);
      }
    }
  }, {
    key: 'renderModalSection',
    value: function renderModalSection() {
      var _props = this.props,
          open = _props.open,
          onClose = _props.onClose,
          backdrop = _props.backdrop,
          size = _props.size,
          aside = _props.aside,
          children = _props.children,
          style = _props.style,
          closeButton = _props.closeButton,
          closeButtonStyle = _props.closeButtonStyle,
          contentContainerStyle = _props.contentContainerStyle;


      if (!open) {
        return null;
      }

      var backdropEl = void 0;

      if (!backdrop) {
        backdropEl = null;
      } else if (backdrop === 'full') {
        backdropEl = _react2.default.createElement('div', {
          key: 'backdrop',
          style: { backgroundColor: '#fff' },
          className: 'Modal--backdrop',
          onClick: this.handleBackdropClick });
      } else {
        backdropEl = _react2.default.createElement('div', { key: 'backdrop', className: 'Modal--backdrop', onClick: this.handleBackdropClick });
      }

      return _react2.default.createElement(
        _reactTransitionGroup.CSSTransition,
        { key: 'modal', classNames: 'Modal', timeout: { enter: 300, exit: 300 } },
        _react2.default.createElement(
          'section',
          { className: 'Modal--container', ref: 'section', onKeyDown: this.handleKeyDown, tabIndex: -1 },
          backdropEl,
          backdrop === 'full' && _react2.default.createElement(CloseButton, {
            closeButton: closeButton,
            style: _extends({ position: 'absolute', top: 20, right: 20 }, closeButtonStyle),
            onClick: onClose }),
          _react2.default.createElement(
            'div',
            {
              style: style,
              className: (0, _classnames2.default)('display-flex justify-content-center', _defineProperty({}, 'Modal--dialog-aside aside-' + aside, aside != false), { 'full-height': size === 'full' }) },
            _react2.default.createElement(
              'div',
              { className: 'Modal--content ' + size, style: contentContainerStyle },
              children
            )
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(
          _Portal2.default,
          null,
          _react2.default.createElement(
            ModalTransition,
            null,
            this.renderModalSection()
          )
        ),
        _react2.default.createElement(_reactFixedPageMs2.default, { fixed: this.props.open })
      );
    }
  }]);

  return Modal;
}(_react.PureComponent);

Modal.Title = ModalTitle;
Modal.Body = ModalBody;
Modal.Footer = ModalFooter;
Modal.propTypes = {
  open: _propTypes2.default.bool.isRequired,
  onClose: _propTypes2.default.func.isRequired,
  backdrop: _propTypes2.default.oneOf([true, false, 'static', 'full']),
  size: _propTypes2.default.oneOf(['xs', 'sm', 'lg', 'full']),
  // the level of stacking to render at
  aside: _propTypes2.default.oneOf([false, 1, 2, 3, 4, 5]),
  style: _propTypes2.default.object,
  closeButtonStyle: _propTypes2.default.object,
  closeButton: _propTypes2.default.node,
  contentContainerStyle: _propTypes2.default.object
};
Modal.defaultProps = {
  backdrop: 'static',
  size: 'sm',
  aside: false
};
exports.default = Modal;