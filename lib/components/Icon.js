'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ICON_SIZES = [null, '1x', 'lg', '2x', '3x', '4x', '5x'];
var ICON_ANIMATES = [null, 'spin', 'pulse'];

var Icon = function (_PureComponent) {
  _inherits(Icon, _PureComponent);

  function Icon() {
    _classCallCheck(this, Icon);

    return _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).apply(this, arguments));
  }

  _createClass(Icon, [{
    key: 'render',
    value: function render() {
      var _cx;

      var _props = this.props,
          name = _props.name,
          size = _props.size,
          fixed = _props.fixed,
          li = _props.li,
          border = _props.border,
          className = _props.className,
          animate = _props.animate,
          rest = _objectWithoutProperties(_props, ['name', 'size', 'fixed', 'li', 'border', 'className', 'animate']);

      return _react2.default.createElement('i', _extends({
        className: (0, _classnames2.default)('fa fa-' + name, (_cx = {
          'fa-border': border,
          'fa-fw': fixed,
          'fa-li': li
        }, _defineProperty(_cx, 'fa-' + size, size), _defineProperty(_cx, 'fa-' + animate, animate), _cx), className)
      }, rest));
    }
  }]);

  return Icon;
}(_react.PureComponent);

Icon.sizes = ICON_SIZES;
Icon.animates = ICON_ANIMATES;
Icon.propTypes = {
  name: _propTypes2.default.string.isRequired,
  size: _propTypes2.default.oneOf(ICON_SIZES),
  fixed: _propTypes2.default.bool,
  li: _propTypes2.default.bool,
  border: _propTypes2.default.bool,
  animate: _propTypes2.default.oneOf(ICON_ANIMATES),
  className: _propTypes2.default.string,
  style: _propTypes2.default.object
};
Icon.defaultProps = {
  size: null,
  fixed: false,
  li: false,
  border: false,
  animate: null,
  className: null,
  style: null
};
exports.default = Icon;