'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactControllables = require('react-controllables');

var _reactControllables2 = _interopRequireDefault(_reactControllables);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Collapsible = function (_Component) {
  _inherits(Collapsible, _Component);

  function Collapsible() {
    _classCallCheck(this, Collapsible);

    return _possibleConstructorReturn(this, (Collapsible.__proto__ || Object.getPrototypeOf(Collapsible)).apply(this, arguments));
  }

  _createClass(Collapsible, [{
    key: 'renderChildren',
    value: function renderChildren() {
      var _props = this.props,
          alwaysRenderChildren = _props.alwaysRenderChildren,
          children = _props.children,
          collapsed = _props.collapsed;


      if (alwaysRenderChildren) {
        var styles = collapsed ? { height: 0, visibility: 'hidden' } : {};
        return _react2.default.createElement(
          'div',
          { style: styles },
          children
        );
      } else {
        return collapsed ? null : children;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          label = _props2.label,
          summary = _props2.summary,
          collapsed = _props2.collapsed,
          onCollapsedChange = _props2.onCollapsedChange,
          className = _props2.className,
          upIcon = _props2.upIcon,
          downIcon = _props2.downIcon,
          summaryClassName = _props2.summaryClassName,
          style = _props2.style,
          collapsedStyle = _props2.collapsedStyle,
          expandedStyle = _props2.expandedStyle;

      var calculatedStyle = Object.assign({}, style, collapsed ? collapsedStyle : expandedStyle);

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)(className), style: calculatedStyle },
        _react2.default.createElement(
          'div',
          { key: 'summary', onClick: function onClick() {
              return onCollapsedChange(!collapsed);
            },
            className: (0, _classnames2.default)('display-flex align-items-center cursor-pointer pd-sm', summaryClassName) },
          _react2.default.createElement(
            'label',
            { className: 'mg-none' },
            label
          ),
          _react2.default.createElement(
            'div',
            {
              className: 'flex-grow-1 flex-shrink-1 mg-left-sm mg-right-sm text-nowrap text-overflow-ellipsis overflow-hidden' },
            _react2.default.createElement(
              'em',
              null,
              collapsed ? summary : null
            )
          ),
          _react2.default.createElement(
            'div',
            null,
            collapsed ? downIcon : upIcon
          )
        ),
        this.renderChildren()
      );
    }
  }]);

  return Collapsible;
}(_react.Component);

Collapsible.propTypes = {
  alwaysRenderChildren: _propTypes2.default.bool,
  label: _propTypes2.default.node.isRequired,
  collapsed: _propTypes2.default.bool.isRequired,
  onCollapsedChange: _propTypes2.default.func.isRequired,
  upIcon: _propTypes2.default.node,
  downIcon: _propTypes2.default.node,
  summary: _propTypes2.default.node,
  summaryClassName: _propTypes2.default.string,
  style: _propTypes2.default.object,
  collapsedStyle: _propTypes2.default.object,
  expandedStyle: _propTypes2.default.object
};
Collapsible.defaultProps = {
  alwaysRenderChildren: false,
  upIcon: _react2.default.createElement(_Icon2.default, { name: 'angle-up' }),
  downIcon: _react2.default.createElement(_Icon2.default, { name: 'angle-down' }),
  summary: null,
  summaryClassName: null
};
exports.default = (0, _reactControllables2.default)(Collapsible, ['collapsed']);