'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CornerAngle = function (_PureComponent) {
  _inherits(CornerAngle, _PureComponent);

  function CornerAngle() {
    _classCallCheck(this, CornerAngle);

    return _possibleConstructorReturn(this, (CornerAngle.__proto__ || Object.getPrototypeOf(CornerAngle)).apply(this, arguments));
  }

  _createClass(CornerAngle, [{
    key: 'getClasses',
    value: function getClasses() {
      var _props = this.props,
          className = _props.className,
          colorClass = _props.colorClass,
          size = _props.size;


      return (0, _classnames2.default)('border-width-' + size, colorClass || null, className);
    }
  }, {
    key: 'getBorderStyles',
    value: function getBorderStyles() {
      var rgb = this.props.rgb;


      var styles = {
        borderTopColor: 'transparent',
        borderRightStyle: 'solid',
        borderTopStyle: 'solid'
      };

      if (rgb) {
        styles.borderRightColor = 'rgb(' + rgb + ')';
      }

      return styles;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', { className: this.getClasses(), style: this.getBorderStyles() });
    }
  }]);

  return CornerAngle;
}(_react.PureComponent);

CornerAngle.propTypes = {
  // direction? Don't need it now, but could be a nice enhancement
  colorClass: _propTypes2.default.string,
  rgb: _propTypes2.default.string,
  size: _propTypes2.default.oneOf(['xs', 'sm', 'md', 'lg', 'xl'])
};
CornerAngle.defaultProps = {
  colorClass: 'primary-color',
  rgb: null,
  size: 'md'
};
exports.default = CornerAngle;