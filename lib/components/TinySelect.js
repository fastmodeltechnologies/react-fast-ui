'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _OnClickOut = require('./OnClickOut');

var _OnClickOut2 = _interopRequireDefault(_OnClickOut);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MENU_CLASSES = 'box-shadow-sm mg-top-sm';

var MENU_STYLE = {
  position: 'absolute',
  textAlign: 'center',
  color: 'black',
  backgroundColor: 'white',
  top: '100%',
  left: '50%',
  zIndex: 1,
  transform: 'translateX(-50%)',
  overflow: 'auto',
  maxHeight: 400
};

var CARET = _react2.default.createElement(_Icon2.default, { name: 'angle-down', style: { marginLeft: 4 } });

var Value = function (_PureComponent) {
  _inherits(Value, _PureComponent);

  function Value() {
    _classCallCheck(this, Value);

    return _possibleConstructorReturn(this, (Value.__proto__ || Object.getPrototypeOf(Value)).apply(this, arguments));
  }

  _createClass(Value, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          option = _props.option,
          labelKey = _props.labelKey,
          rest = _objectWithoutProperties(_props, ['option', 'labelKey']);

      return _react2.default.createElement(
        'span',
        rest,
        option[labelKey]
      );
    }
  }]);

  return Value;
}(_react.PureComponent);

Value.propTypes = {
  option: _propTypes2.default.object.isRequired,
  labelKey: _propTypes2.default.string.isRequired
};

var TinySelect = function (_PureComponent2) {
  _inherits(TinySelect, _PureComponent2);

  function TinySelect() {
    var _ref;

    var _temp, _this2, _ret;

    _classCallCheck(this, TinySelect);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this2 = _possibleConstructorReturn(this, (_ref = TinySelect.__proto__ || Object.getPrototypeOf(TinySelect)).call.apply(_ref, [this].concat(args))), _this2), _this2.state = {
      open: false
    }, _this2.handleChange = function (option) {
      var onChange = _this2.props.onChange;

      _this2.setState({ open: false }, function () {
        return onChange(option);
      });
    }, _this2.handleClickOut = function () {
      _this2.setState({ open: false });
    }, _this2.handleClickValue = function () {
      var open = _this2.state.open;

      _this2.setState({ open: !open });
    }, _temp), _possibleConstructorReturn(_this2, _ret);
  }

  _createClass(TinySelect, [{
    key: 'renderOptions',
    value: function renderOptions() {
      var _this3 = this;

      var _props2 = this.props,
          options = _props2.options,
          labelKey = _props2.labelKey,
          valueKey = _props2.valueKey,
          showSelected = _props2.showSelected,
          value = _props2.value;
      var open = this.state.open;


      if (!open) {
        return null;
      }

      return _react2.default.createElement(
        'div',
        { className: MENU_CLASSES, style: MENU_STYLE },
        options.map(function (option, index) {
          var isSelected = value && option[valueKey] == value[valueKey];

          if (isSelected && !showSelected) {
            return null;
          }

          var className = (0, _classnames2.default)('pd-sm text-nowrap bg-color-white hover-bg-color-lightest-gray', { 'border-bottom-color-gray-background-2px': index < options.length - 1 });

          return _react2.default.createElement(
            'div',
            { className: className, key: option[valueKey], onClick: function onClick() {
                return _this3.handleChange(option);
              } },
            _react2.default.createElement(Value, { labelKey: labelKey, option: option })
          );
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          className = _props3.className,
          style = _props3.style,
          placeholder = _props3.placeholder,
          value = _props3.value,
          labelKey = _props3.labelKey;


      return _react2.default.createElement(
        _OnClickOut2.default,
        { onClickOut: this.handleClickOut },
        _react2.default.createElement(
          'span',
          { style: style, className: (0, _classnames2.default)('cursor-pointer position-relative', className) },
          _react2.default.createElement(
            'span',
            { onClick: this.handleClickValue, className: 'text-nowrap' },
            value ? _react2.default.createElement(Value, { labelKey: labelKey, option: value }) : placeholder,
            CARET
          ),
          this.renderOptions()
        )
      );
    }
  }]);

  return TinySelect;
}(_react.PureComponent);

TinySelect.propTypes = {
  onChange: _propTypes2.default.func.isRequired,
  options: _propTypes2.default.arrayOf(_propTypes2.default.object).isRequired,
  value: _propTypes2.default.any,
  labelKey: _propTypes2.default.string,
  valueKey: _propTypes2.default.string,
  showSelected: _propTypes2.default.bool,
  placeholder: _propTypes2.default.node
};
TinySelect.defaultProps = {
  placeholder: _react2.default.createElement(
    'em',
    null,
    'Select...'
  ),
  labelKey: 'label',
  valueKey: 'value',
  showSelected: true
};
exports.default = TinySelect;