'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SimpleButton = require('./SimpleButton');

var _SimpleButton2 = _interopRequireDefault(_SimpleButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var getColorClasses = function getColorClasses(type) {
  return 'color-' + type + ' hover-color-' + type + '-darken';
};

var LinkButton = function (_PureComponent) {
  _inherits(LinkButton, _PureComponent);

  function LinkButton() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, LinkButton);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = LinkButton.__proto__ || Object.getPrototypeOf(LinkButton)).call.apply(_ref, [this].concat(args))), _this), _this.beforeOnClick = function (e) {
      return _this.props.onClick && !_this.props.disabled ? _this.props.onClick(e) : null;
    }, _this.focus = function () {
      return _this.refs.button.focus();
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(LinkButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          className = _props.className,
          disabled = _props.disabled,
          href = _props.href,
          size = _props.size,
          style = _props.style,
          type = _props.type;

      var classNames = (0, _classnames2.default)(disabled ? 'color-light-gray hover-color-light-gray' : getColorClasses(type), size + '-font', className);
      var styles = _extends({ textDecoration: 'none', cursor: disabled ? 'not-allowed' : 'pointer' }, style);

      return _react2.default.createElement(
        'span',
        {
          className: classNames,
          style: styles,
          ref: 'button',
          onClick: this.beforeOnClick },
        children
      );
    }
  }]);

  return LinkButton;
}(_react.PureComponent);

LinkButton.sizes = _SimpleButton2.default.sizes;
LinkButton.types = _SimpleButton2.default.types;
LinkButton.propTypes = {
  disabled: _propTypes2.default.bool,
  href: _propTypes2.default.string,
  onClick: _propTypes2.default.func,
  size: _propTypes2.default.oneOf(LinkButton.sizes),
  tabIndex: _propTypes2.default.string,
  to: _propTypes2.default.string,
  type: _propTypes2.default.oneOf(LinkButton.types)
};
LinkButton.defaultProps = {
  disabled: false,
  href: null,
  onClick: null,
  size: 'lg',
  tabIndex: null,
  to: null,
  type: 'primary'
};
exports.default = LinkButton;