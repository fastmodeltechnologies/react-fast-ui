'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ICON_SIZE_MAP = {
  xs: '',
  sm: '1x',
  md: 'lg',
  lg: '2x',
  xl: '3x'
};

var SIZES = ['xs', 'sm', 'md', 'lg', 'xl'];

var IconButton = function (_Component) {
  _inherits(IconButton, _Component);

  function IconButton() {
    _classCallCheck(this, IconButton);

    return _possibleConstructorReturn(this, (IconButton.__proto__ || Object.getPrototypeOf(IconButton)).apply(this, arguments));
  }

  _createClass(IconButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          color = _props.color,
          colorOverride = _props.colorOverride,
          disabled = _props.disabled,
          fontSize = _props.fontSize,
          icon = _props.icon,
          iconSize = _props.iconSize,
          onClick = _props.onClick,
          text = _props.text;

      var parentClasses = (0, _classnames2.default)('align-items-center', { 'cursor-pointer': !disabled }, { 'color-light-gray': !colorOverride || disabled }, 'display-flex', 'flex-direction-column', _defineProperty({}, 'hover-color-' + color, !disabled), 'hover-visibility-parent', className);
      var textClasses = (0, _classnames2.default)(fontSize + '-font', 'mg-top-' + fontSize, 'visibility-hidden', 'visibility-child-visible');

      return _react2.default.createElement(
        'div',
        { className: parentClasses, onClick: disabled ? null : onClick },
        _react2.default.createElement(_Icon2.default, { name: icon, size: ICON_SIZE_MAP[iconSize] }),
        text ? _react2.default.createElement(
          'div',
          { className: textClasses },
          text
        ) : null
      );
    }
  }]);

  return IconButton;
}(_react.Component);

IconButton.propTypes = {
  color: _propTypes2.default.string,
  colorOverride: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  fontSize: _propTypes2.default.oneOf(SIZES),
  icon: _propTypes2.default.string.isRequired,
  iconSize: _propTypes2.default.oneOf(SIZES),
  onClick: _propTypes2.default.func.isRequired,
  text: _propTypes2.default.string
};
IconButton.defaultProps = {
  color: 'primary',
  colorOverride: false,
  disabled: false,
  fontSize: 'xs',
  iconSize: 'md',
  text: null
};
exports.default = IconButton;