'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Spinner = require('./Spinner');

var _Spinner2 = _interopRequireDefault(_Spinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FILL_RELATIVE_CONTAINER = { position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 };
var LOADING_SCRIM = _react2.default.createElement('div', { style: _extends({}, FILL_RELATIVE_CONTAINER, { opacity: 0.8, backgroundColor: 'white' }) });

var MID_SPINNER = _react2.default.createElement(
  'div',
  { style: FILL_RELATIVE_CONTAINER,
    className: 'display-flex align-items-center justify-content-center' },
  _react2.default.createElement(_Spinner2.default, null)
);

var LoadingWrapper = function (_PureComponent) {
  _inherits(LoadingWrapper, _PureComponent);

  function LoadingWrapper() {
    _classCallCheck(this, LoadingWrapper);

    return _possibleConstructorReturn(this, (LoadingWrapper.__proto__ || Object.getPrototypeOf(LoadingWrapper)).apply(this, arguments));
  }

  _createClass(LoadingWrapper, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          loading = _props.loading,
          loadingScrim = _props.loadingScrim,
          spinner = _props.spinner,
          style = _props.style,
          children = _props.children,
          rest = _objectWithoutProperties(_props, ['loading', 'loadingScrim', 'spinner', 'style', 'children']);

      return _react2.default.createElement(
        'div',
        _extends({ style: _extends({}, style, { position: 'relative' }) }, rest),
        children,
        loading ? _react2.default.createElement(
          'span',
          null,
          loadingScrim,
          spinner
        ) : null
      );
    }
  }]);

  return LoadingWrapper;
}(_react.PureComponent);

LoadingWrapper.propTypes = {
  loading: _propTypes2.default.bool.isRequired,
  loadingScrim: _propTypes2.default.node,
  spinner: _propTypes2.default.node
};
LoadingWrapper.defaultProps = {
  loadingScrim: LOADING_SCRIM,
  spinner: MID_SPINNER
};
exports.default = LoadingWrapper;