'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VALUE_SHAPE = _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.bool, _propTypes2.default.number]);
var OPTION_SHAPE = _propTypes2.default.shape({
  label: _propTypes2.default.node.isRequired,
  value: VALUE_SHAPE.isRequired
});

var DEMO_OPTIONS = [{ label: _react2.default.createElement(_Icon2.default, { name: 'user' }), value: 'option 1' }, { label: 2, value: 'option 2' }, { label: _react2.default.createElement(
    'span',
    { className: 'color-fastmodel-red' },
    'Option 3'
  ), value: 'option 3' }];

// common option cx
var COMMON_CLASSNAMES = (0, _classnames2.default)('border-style-solid', 'border-width-1px', 'cursor-pointer', 'display-inline-block', 'pd-top-sm', 'pd-bottom-sm', 'pd-left-md', 'pd-right-md');

// classname for selected option
var SELECTED_CLASSNAME = (0, _classnames2.default)('bg-color-rich-orange', 'hover-bg-color-rich-orange-darken', 'color-white', 'border-color-rich-orange-darken');

// classname for non-selected option
var NON_SELECTED_CLASSNAME = (0, _classnames2.default)('bg-color-white', 'hover-bg-color-lightest-gray', 'color-light-gray', 'border-color-light-gray');

// classname for first option
var FIRST_CLASSNAME = (0, _classnames2.default)('border-top-left-radius-sm', 'border-bottom-left-radius-sm');

// classname for last option
var LAST_CLASSNAME = (0, _classnames2.default)('border-top-right-radius-sm', 'border-bottom-right-radius-sm');

var Option = function (_Component) {
  _inherits(Option, _Component);

  function Option() {
    _classCallCheck(this, Option);

    return _possibleConstructorReturn(this, (Option.__proto__ || Object.getPrototypeOf(Option)).apply(this, arguments));
  }

  _createClass(Option, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          first = _props.first,
          last = _props.last,
          onClick = _props.onClick,
          option = _props.option,
          selected = _props.selected;

      var className = (0, _classnames2.default)(COMMON_CLASSNAMES, first ? FIRST_CLASSNAME : null, last ? LAST_CLASSNAME : null, selected ? SELECTED_CLASSNAME : NON_SELECTED_CLASSNAME);

      return _react2.default.createElement(
        'div',
        { className: className, onClick: onClick },
        option.label
      );
    }
  }]);

  return Option;
}(_react.Component);

Option.propsTypes = {
  first: _propTypes2.default.bool.isRequired,
  last: _propTypes2.default.bool.isRequired,
  onClick: _propTypes2.default.func.isRequired,
  option: OPTION_SHAPE,
  selected: _propTypes2.default.bool.isRequired
};

var ButtonToggle = function (_Component2) {
  _inherits(ButtonToggle, _Component2);

  function ButtonToggle() {
    _classCallCheck(this, ButtonToggle);

    return _possibleConstructorReturn(this, (ButtonToggle.__proto__ || Object.getPrototypeOf(ButtonToggle)).apply(this, arguments));
  }

  _createClass(ButtonToggle, [{
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          className = _props2.className,
          onChange = _props2.onChange,
          options = _props2.options,
          value = _props2.value;


      return _react2.default.createElement(
        'span',
        { className: className },
        options.map(function (opt, i) {
          return _react2.default.createElement(Option, {
            key: i,
            first: i == 0,
            last: i == options.length - 1,
            onClick: function onClick() {
              return onChange(opt.value);
            },
            option: opt,
            selected: opt.value === value
          });
        })
      );
    }
  }]);

  return ButtonToggle;
}(_react.Component);

ButtonToggle.propTypes = {
  onChange: _propTypes2.default.func.isRequired,
  options: _propTypes2.default.arrayOf(OPTION_SHAPE),
  value: VALUE_SHAPE.isRequired
};
exports.default = ButtonToggle;