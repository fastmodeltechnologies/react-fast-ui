'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _HeaderCell = require('./table/HeaderCell');

var _HeaderCell2 = _interopRequireDefault(_HeaderCell);

var _TableCell = require('./table/TableCell');

var _TableCell2 = _interopRequireDefault(_TableCell);

var _TableHead = require('./table/TableHead');

var _TableHead2 = _interopRequireDefault(_TableHead);

var _TableBody = require('./table/TableBody');

var _TableBody2 = _interopRequireDefault(_TableBody);

var _TableRow = require('./table/TableRow');

var _TableRow2 = _interopRequireDefault(_TableRow);

var _reactDimensions = require('react-dimensions');

var _reactDimensions2 = _interopRequireDefault(_reactDimensions);

var _TableContext = require('./table/TableContext');

var _TableContext2 = _interopRequireDefault(_TableContext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Table = function (_Component) {
  _inherits(Table, _Component);

  function Table(props) {
    _classCallCheck(this, Table);

    var _this = _possibleConstructorReturn(this, (Table.__proto__ || Object.getPrototypeOf(Table)).call(this, props));

    _this.state = {
      contextValues: {
        containerWidth: props.containerWidth,
        maxWidth: props.maxWidth,
        gutterClassName: props.gutterClassName
      }
    };
    return _this;
  }

  _createClass(Table, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _props = this.props,
          prevWidth = _props.containerWidth,
          prevMax = _props.maxWidth,
          prevGutter = _props.gutterClassName;
      var containerWidth = nextProps.containerWidth,
          maxWidth = nextProps.maxWidth,
          gutterClassName = nextProps.gutterClassName;


      if (containerWidth !== prevWidth || maxWidth !== prevMax || gutterClassName !== prevGutter) {
        this.setState({ contextValues: { containerWidth: containerWidth, maxWidth: maxWidth, gutterClassName: gutterClassName } });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          style = _props2.style,
          wrapperStyle = _props2.wrapperStyle,
          responsive = _props2.responsive,
          containerWidth = _props2.containerWidth,
          maxWidth = _props2.maxWidth,
          gutterClassName = _props2.gutterClassName,
          rest = _objectWithoutProperties(_props2, ['style', 'wrapperStyle', 'responsive', 'containerWidth', 'maxWidth', 'gutterClassName']);

      var table = _react2.default.createElement(
        _TableContext2.default.Provider,
        { value: this.state.contextValues },
        _react2.default.createElement('table', _extends({ style: _extends({ width: '100%' }, style) }, rest))
      );

      if (!responsive) {
        return table;
      }

      return _react2.default.createElement(
        'div',
        { style: _extends({ overflow: 'auto', width: '100%', WebkitOverflowScrolling: 'touch' }, wrapperStyle) },
        table
      );
    }
  }]);

  return Table;
}(_react.Component);

Table.propTypes = {
  containerWidth: _propTypes2.default.number.isRequired,
  containerHeight: _propTypes2.default.number.isRequired,
  maxWidth: _propTypes2.default.number,

  responsive: _propTypes2.default.bool,
  gutterClassName: _propTypes2.default.string,
  wrapperStyle: _propTypes2.default.object
};
Table.defaultProps = {
  maxWidth: 1400,
  responsive: true,
  gutterClassName: 'hidden-print'
};
exports.default = Object.assign((0, _reactDimensions2.default)()(Table), {
  demo: Table.demo,
  thead: _TableHead2.default,
  td: _TableCell2.default,
  th: _HeaderCell2.default,
  tbody: _TableBody2.default,
  tr: _TableRow2.default
});