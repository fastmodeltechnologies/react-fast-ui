'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SUB_TAB_STYLES = exports.TAB_STYLES = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _LinkButton = require('./LinkButton');

var _LinkButton2 = _interopRequireDefault(_LinkButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TAB_KEY_TYPE = _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]);

var TAB_STYLES = exports.TAB_STYLES = {
  containerStyle: {},
  listStyle: { borderBottom: '1px solid #cecece' },
  listCX: 'list-unstyled pd-none hidden-print mg-bottom-none',
  labelLICX: 'display-inline-block',
  labelLIStyle: { marginRight: 30, padding: '0 10px 8px 10px', position: 'relative', left: '-10px' },
  selectedCX: 'border-bottom-width-4px border-bottom-style-solid border-bottom-color-rich-orange',
  selectedStyle: { color: '#0a0a0a', fontWeight: 800 },
  linkCX: 'color-gray-background-darken',
  linkStyle: {},
  selectedLinkCX: ''
};

var SUB_TAB_STYLES = exports.SUB_TAB_STYLES = {
  containerStyle: {
    width: '100vw',
    position: 'relative',
    left: '50%',
    marginLeft: '-50vw',
    backgroundColor: '#ececec',
    display: 'flex',
    justifyContent: 'center',
    borderBottom: '1px solid #cecece',
    borderTop: '1px solid #cecece'
  },
  listStyle: {},
  listCX: 'list-unstyled pd-sm hidden-print mg-bottom-none',
  labelLICX: 'display-inline-block pd-top-sm pd-bottom-sm',
  labelLIStyle: { paddingRight: '50px' },
  selectedCX: 'pd-none color-orange-darken hover-color-orange-darken font-weight-bolder',
  selectedStyle: {},
  linkCX: 'pd-none color-primary hover-color-orange-darken',
  linkStyle: {},
  selectedLinkCX: 'pd-none color-orange-darken hover-color-orange-darken font-weight-bolder'
};

var Tab = function (_PureComponent) {
  _inherits(Tab, _PureComponent);

  function Tab() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Tab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Tab.__proto__ || Object.getPrototypeOf(Tab)).call.apply(_ref, [this].concat(args))), _this), _this.isSelected = function () {
      return _underscore2.default.isEqual(_this.props.tabKey, _this.props.selectedKey);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Tab, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          children = _props.children,
          style = _props.style;


      return !this.isSelected() ? null : _react2.default.createElement(
        'div',
        { className: className, style: style },
        _react.Children.only(children)
      );
    }
  }]);

  return Tab;
}(_react.PureComponent);

Tab.propTypes = {
  disabled: _propTypes2.default.bool,
  selectedKey: TAB_KEY_TYPE,
  tabKey: TAB_KEY_TYPE
};
Tab.defaultProps = {
  disabled: false
};

var Tabs = function (_PureComponent2) {
  _inherits(Tabs, _PureComponent2);

  function Tabs() {
    var _ref2;

    var _temp2, _this2, _ret2;

    _classCallCheck(this, Tabs);

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, (_ref2 = Tabs.__proto__ || Object.getPrototypeOf(Tabs)).call.apply(_ref2, [this].concat(args))), _this2), _this2.handleClickTab = function (tabKey) {
      var onSelect = _this2.props.onSelect;

      return onSelect ? onSelect(tabKey) : null;
    }, _temp2), _possibleConstructorReturn(_this2, _ret2);
  }

  _createClass(Tabs, [{
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props2 = this.props,
          className = _props2.className,
          children = _props2.children,
          selectedKey = _props2.selectedKey,
          listStyle = _props2.listStyle,
          isSubTabs = _props2.isSubTabs,
          style = _props2.style;

      var STYLE = isSubTabs ? SUB_TAB_STYLES : TAB_STYLES;

      return _react2.default.createElement(
        'div',
        { style: style },
        _react2.default.createElement(
          'div',
          { style: STYLE.containerStyle },
          _react2.default.createElement(
            'ul',
            {
              className: (0, _classnames2.default)(STYLE.listCX, className),
              style: _extends({}, STYLE.listStyle, listStyle) },
            _react.Children.map(children, function (child, ix) {
              if (!child) {
                return null;
              }
              var _child$props = child.props,
                  disabled = _child$props.disabled,
                  href = _child$props.href,
                  label = _child$props.label,
                  style = _child$props.style,
                  tabKey = _child$props.tabKey;

              var isSelected = selectedKey === tabKey;
              var onClick = disabled ? null : function () {
                return _this3.handleClickTab(tabKey);
              };
              var className = isSelected ? STYLE.selectedLinkCX : STYLE.linkCX;

              return _react2.default.createElement(
                'li',
                {
                  key: ix,
                  className: (0, _classnames2.default)(STYLE.labelLICX, _defineProperty({}, STYLE.selectedCX, isSelected)),
                  style: _extends({}, STYLE.labelLIStyle, isSelected ? STYLE.selectedStyle : null, style) },
                _react2.default.createElement(
                  _LinkButton2.default,
                  {
                    disabled: disabled,
                    href: href,
                    onClick: onClick,
                    size: 'lg',
                    className: className },
                  label
                )
              );
            })
          )
        ),
        _react.Children.map(children, function (child) {
          if (!child) {
            return null;
          }
          return (0, _react.cloneElement)(child, { selectedKey: selectedKey });
        })
      );
    }
  }]);

  return Tabs;
}(_react.PureComponent);

Tabs.Tab = Tab;
Tabs.propTypes = {
  onSelect: _propTypes2.default.func,
  selectedKey: TAB_KEY_TYPE,
  isSubTabs: _propTypes2.default.bool
};
Tabs.defaultProps = {
  onSelect: null,
  selectedKey: null,
  isSubTabs: false
};
exports.default = Tabs;