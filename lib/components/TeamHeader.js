'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _TeamLogo = require('./TeamLogo');

var _TeamLogo2 = _interopRequireDefault(_TeamLogo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HEADER_CLASSES = (0, _classnames2.default)('display-flex justify-content-space-between', 'flex-shrink-0 full-height');

var getGradientString = function getGradientString(primaryColor) {
  return 'linear-gradient(to right, #' + primaryColor + ', #' + primaryColor + '80)';
};

var TeamHeader = function (_Component) {
  _inherits(TeamHeader, _Component);

  function TeamHeader() {
    _classCallCheck(this, TeamHeader);

    return _possibleConstructorReturn(this, (TeamHeader.__proto__ || Object.getPrototypeOf(TeamHeader)).apply(this, arguments));
  }

  _createClass(TeamHeader, [{
    key: 'render',


    // lifecycle
    value: function render() {
      var _props = this.props,
          className = _props.className,
          children = _props.children,
          style = _props.style,
          primaryColor = _props.primaryColor,
          fullName = _props.fullName,
          logo = _props.logo,
          teamRecord = _props.teamRecord,
          showConferenceRecord = _props.showConferenceRecord;


      var headerStyle = { height: 132, width: '100%', backgroundColor: '#000', top: 0, zIndex: -1 };

      return _react2.default.createElement(
        'header',
        { style: { position: 'relative', height: 132 } },
        _react2.default.createElement('div', {
          className: 'hidden-print position-absolute',
          style: _extends({}, headerStyle, {
            backgroundImage: getGradientString(primaryColor)
          }) }),
        _react2.default.createElement(
          'div',
          { className: className, style: _extends({ height: 132 }, style) },
          _react2.default.createElement(
            'section',
            { style: { margin: 'auto', paddingLeft: 44, paddingRight: 44 }, className: HEADER_CLASSES },
            _react2.default.createElement(
              'div',
              { className: 'display-flex flex-direction-column justify-content-flex-end full-height' },
              _react2.default.createElement(
                'h1',
                {
                  className: 'mg-none whitespace-nowrap print-color-white futura-pt-bold text-transform-uppercase',
                  style: { marginTop: 0, marginBottom: 0, fontSize: 50 } },
                fullName,
                showConferenceRecord && _react2.default.createElement(
                  'span',
                  { className: 'futura-pt-md', style: { fontSize: 22, paddingLeft: 10 } },
                  teamRecord
                )
              ),
              children
            ),
            _react2.default.createElement(
              'div',
              { className: 'display-flex flex-direction-column justify-content-center full-height' },
              _react2.default.createElement('div', { style: { backgroundImage: 'url(' + logo } }),
              _react2.default.createElement(_TeamLogo2.default, { url: logo, height: 100 })
            )
          )
        )
      );
    }
  }]);

  return TeamHeader;
}(_react.Component);

TeamHeader.contextTypes = {
  contentWidth: _propTypes2.default.number
};
TeamHeader.propTypes = {
  primaryColor: _propTypes2.default.string,
  fullName: _propTypes2.default.string,
  logo: _propTypes2.default.string,
  showConferenceRecord: _propTypes2.default.bool,
  // Insert Component to show conference record
  teamRecord: _propTypes2.default.any
};
TeamHeader.defaultProps = {
  showConferenceRecord: false,
  primaryColor: '#000000',
  fullName: '',
  logo: false
};
exports.default = TeamHeader;