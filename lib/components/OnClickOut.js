'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Tests if element is contained within the testParent or has the required class.
 * @param child to test for containment
 * @param parent to see if contains child
 * @returns {boolean} true if child is equal to or contained within testParent
 */
function isContained(child, parent, className) {
  // while element points to a valid element
  while (child) {
    // if the element is the parent we are testing for, then it's contained within the testParent
    if (child === parent || className && child.classList.contains(className)) {
      return true;
    }
    child = child.parentElement;
  }

  return false;
}

var OnClickOut = function (_Component) {
  _inherits(OnClickOut, _Component);

  function OnClickOut() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, OnClickOut);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = OnClickOut.__proto__ || Object.getPrototypeOf(OnClickOut)).call.apply(_ref, [this].concat(args))), _this), _this.windowClickListener = function (clickEvent) {
      var _this$props = _this.props,
          onClickOut = _this$props.onClickOut,
          fireOnUnmountedDOM = _this$props.fireOnUnmountedDOM,
          portalClass = _this$props.portalClass;

      // find the DOM node corresponding to this component

      var me = (0, _reactDom.findDOMNode)(_this);

      // check if the target of the click event is within this DOM node
      var target = clickEvent.target;

      var inPage = isContained(target, document.documentElement);

      if (!isContained(target, me, portalClass) && (fireOnUnmountedDOM || inPage)) {
        onClickOut(clickEvent);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(OnClickOut, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.children !== this.props.children;
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var eventType = this.props.eventType;

      window.addEventListener(eventType, this.windowClickListener);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      var eventType = this.props.eventType;

      window.removeEventListener(eventType, this.windowClickListener);
    }
  }, {
    key: 'render',
    value: function render() {
      var children = this.props.children;

      return _react.Children.count(children) === 1 ? _react.Children.only(children) : _react2.default.createElement(
        'div',
        null,
        children
      );
    }
  }]);

  return OnClickOut;
}(_react.Component);

OnClickOut.isContained = isContained;
OnClickOut.propTypes = {
  onClickOut: _propTypes2.default.func.isRequired,
  // whether the onClickOut event should fire if the target of the event is not in the DOM by the time the event
  // makes it to the window object
  fireOnUnmountedDOM: _propTypes2.default.bool,

  // An optional class that can be provided to check for.  Allows use of popover/portal component that will fail
  // the isContained checking purely parentElements.
  portalClass: _propTypes2.default.string,

  eventType: _propTypes2.default.oneOf(['click', 'mousedown', 'mouseup'])
};
OnClickOut.defaultProps = {
  eventType: 'click',
  fireOnUnmountedDOM: false
};
exports.default = OnClickOut;