'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DISPLAY_NONE = { display: 'none' };

var Form = function (_Component) {
  _inherits(Form, _Component);

  function Form() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Form);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Form.__proto__ || Object.getPrototypeOf(Form)).call.apply(_ref, [this].concat(args))), _this), _this.state = { submitting: false }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Form, [{
    key: 'handleSubmit',
    value: function handleSubmit(e) {
      var onSubmit = this.props.onSubmit;

      // form submission's default action of going to the URL is always prevented for this form component

      e.preventDefault();
      e.stopPropagation();

      // Call onSubmit callback
      onSubmit(e);
    }

    // public function to trigger a submit on the form

  }, {
    key: 'submit',
    value: function submit() {
      var _this2 = this;

      var submitting = this.state.submitting;


      if (!submitting) {
        this.setState({ submitting: true }, function () {
          var _tempSubmitBtn = _this2.refs._tempSubmitBtn;

          if (_tempSubmitBtn) {
            (0, _reactDom.findDOMNode)(_tempSubmitBtn).click();
            _this2.setState({ submitting: false });
          }
        });
      }
    }
  }, {
    key: 'renderAutocompletePrevention',
    value: function renderAutocompletePrevention() {
      var autoComplete = this.props.autoComplete;


      if (autoComplete) {
        return null;
      }

      return _react2.default.createElement(
        'span',
        { key: 'no-autocomplete' },
        _react2.default.createElement('input', { key: '_ac_text', type: 'text', style: DISPLAY_NONE }),
        _react2.default.createElement('input', { key: '_ac_pass', type: 'password', style: DISPLAY_NONE })
      );
    }
  }, {
    key: 'renderHiddenSubmit',
    value: function renderHiddenSubmit() {
      var submitting = this.state.submitting;


      if (!submitting) {
        return null;
      }

      return _react2.default.createElement('input', { key: 'hidden-submit', type: 'submit', ref: '_tempSubmitBtn', style: DISPLAY_NONE });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          children = _props.children,
          className = _props.className,
          autoComplete = _props.autoComplete;


      return _react2.default.createElement(
        'form',
        _extends({ onSubmit: function onSubmit(e) {
            return _this3.handleSubmit(e);
          } }, { className: className, autoComplete: autoComplete }),
        this.renderAutocompletePrevention(),
        _react2.default.createElement(
          'span',
          { key: 'children' },
          children
        ),
        this.renderHiddenSubmit()
      );
    }
  }]);

  return Form;
}(_react.Component);

Form.propTypes = {
  autoComplete: _propTypes2.default.bool,
  onSubmit: _propTypes2.default.func.isRequired
};
Form.defaultProps = {
  autoComplete: true
};
exports.default = Form;